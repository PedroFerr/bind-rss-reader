/**
 * Case Study
 * (please read README.md doc. Thank you!
 *
 * Dec 2017
 * Pedro dot Ferreira at Miles-NET dot com
 */

// ---------------------------
//      LOADING STUFF
// ---------------------------

// On loading any Img, the loading wheel gets loaded, in front. Once img is loaded, fade loading wheel away!
//Remmeber the HTML, and the pulling of live data, comes from a JSON, loaded as a file - as if coming from the Web, but a little bit 

//So, DOM is ready not before do_itemsGrid() and do_headerItems() is called and finish.

$(document).ready(function(){
    const
        $eachContainer = $('div.container-of-feeds > div.article-col')
        //The collection of all imgs:
        , $eachImg = $eachContainer.find('img')
        //... and each of the belongign loading wheels:
        , $eachLoadingWheel = $eachContainer.children('.headers-preloader:first-child')

    ;
    //Being at DOM but BEFORE everything is loaded... hide loading wheels, were activated:
    $eachImg.each(function(){
        const
            $thisImg = $(this)

        ;
        $thisImg
            .on('load', function(){
                $eachLoadingWheel.hide(500);
            })
            .attr('src', $thisImg.attr('src'))  //If img comes from cache, src is already set and will throw an error. Changing it's 'src' (to the same! Yes!) resets the original source
        ; 
    })
    
})

// ---------------------------
//      INIT
// ---------------------------
//Let's parse the SML URL to an Obj:
const
    objParsed = xmlParser('https://www.engadget.com/rss-full.xml');

;
//console.error(objParsed);

//Make sure all data, files, etc, are completely and entire loaded:
//$(window).on('load', function () {
    //Echo Obj @ data/json.js:
    //console.log(rssContent);

    //OK. Here we go!
    //Give it a go to the items we have: get the LATEST 10 items (sort date desc => pick 1st 10), as the trial demands:
    const
        items          = rssContent.rss.channel.item
        , max_nItems   = 10
        //The property where date is, on each item:
        , date_field   = 'pubDate'
        //The name of an extra property, we'll add to each of 'items', having date (as a string) translated to a JS date format:
        , JSdate_field = 'js_date'
    ;
    var
        //An array where all 'items' will be collected for later HTML printing:
        arrayItems = []

    ;

    //1st, to sort after, you need to get each item into an array:
    arrayItems = add_JSDateProp(items, date_field, JSdate_field);
    
    //Sort it, desc, by field 'JSdate_field':
    sortByProperty(arrayItems, JSdate_field);
    /*
    arrayItems.sort(JSdate_field, function(a,b){
        return a[JSdate_field] <=  b[JSdate_field] ? 1 : -1;        
    });
    */

    //Only want 1st 10...?
    arrayItems = arrayItems.slice(0, max_nItems);
    //console.warn(arrayItems);

    //Do each grid cell with each of 'arrayItems' item, already sorted and sliced to Top 'max_nItems':
    do_itemsGrid(arrayItems);

    //And finish with other pulled (common) elements:
    do_headerItems(rssContent);

//})

// ---------------------------
//      BINDINGS
// ---------------------------

//Let the reader view each article, on a full page Modal once called/to be open:
$('#bs-modal').on('show.bs.modal', onShowModal_printArticle);




// ---------------------------
//      AUX FUNCTIONS
// ---------------------------
/**
 * Adds a new JS Date property, named 'JSdateProp', from a 'dateProp' string Date, on a 'dateObj' Object
 * 
 * @param {Obj} dateObj         // An Obj with 'date_field' as a date property 
 * @param {string} dateProp     // The date property name, passing in as a string
 * @param {date} JSdateProp     // The JS date property name, that will be added to each of 'dateObj' ('items' collection)
 */
function add_JSDateProp(dateObj, dateProp, JSdateProp){
    var
        arrayDates = []
    ;

    //Transverse items: 
    Object.keys(dateObj).forEach(function(item_key, item_idx) {
        const
            eachItem  = dateObj[item_key]
            , strDate = eachItem[dateProp]
            //And it's parts, so we can convert it to a JS date to compare (latest!)
            , strDate_date = strDate.split(',')[1].trim()
            , jsDate       = new Date(strDate_date)

        ;
        //console.log(eachItem);
        //Add a new property to each: a JS date (NOTE: JS translates to GMT: "Mon, 04 Dec 2017 08:31:00 -0500" => Mon Dec 04 2017 13:31:00 GMT+0000 (Hora padrão de GMT))
        eachItem[JSdateProp] = jsDate
        //Do an array with each, so it can be sorted after:
        arrayDates.push(eachItem)

    });

    return arrayDates;

}


/**
 * Sorts an 'array' of Objs, by given property 'propertyName' of those Obj collection
 * 
 * @param {array} array 
 * @param {string} propertyName
 */
function sortByProperty(array, propertyName) {
    return array.sort(function (a, b) {
        return b[propertyName] - a[propertyName];
    });
}


/**
 * Draw the HTML's grid of the collection of feeds (fetched in 'items' collection ok Objs) from 'arrayFeeds'
 * 
 * @param {array} arrayFeeds
 */
function do_itemsGrid(arrayFeeds){
    const
        $grid = $('main div.container-of-feeds')
        
    ;

    //Transverse each feed:
    for (var nCell = 0; nCell < arrayFeeds.length; nCell++) {
        const
            gridCell      = arrayFeeds[nCell]
            , description = gridCell.description.__cdata
            , title       = gridCell.title.__cdata
            , linkTitle   = gridCell.link
            , pubDate     = gridCell.pubDate
            , author      = gridCell.creator.__cdata
            , commentsURL = gridCell.comments
            //Collection of categories:
            , categories  = gridCell.category

            //Concerning DOM, we will have a Description, containing an Img:
            , $description = $(description)
            , $desc_img    = $description.filter('img')
            //and a collection of paragraphs, below each Img, also inside each Description:
            , $desc_p      = $description.filter('p')
        
        ;
        //Must collect outerHTML of each of the colection above:
        var
            desc_p_HTML       = ''
            , categories_HTML = ''
        ;
        $desc_p.each(function(){
            desc_p_HTML += $(this)[0].outerHTML;
        })
        categories.forEach(function(elem){
            const
                randomNumber = Math.floor(Math.random()*6)

            ;
            var
                strBadgeClass = ''
            
            ;
            switch(randomNumber) {
                case 0:
                   strBadgeClass = 'label-default'
                   break;
                case 1:
                   strBadgeClass = 'label-primary'
                   break;
                case 2:
                   strBadgeClass = 'label-success'
                   break;
                case 3:
                   strBadgeClass = 'label-info'
                   break;
                case 4:
                   strBadgeClass = 'label-warning'
                   break;
                case 5:
                   strBadgeClass = 'label-danger'
                   break;
            } 
            categories_HTML += '<a href="JavaScript: alert(\'Filter by ' +elem+ '\');" class="label ' +strBadgeClass+ '">' +elem+ '</a>';
        })
        //console.warn(desc_p_HTML);

        //Do each cell (a BS column) with a BS panel collapse inside:
        $grid.append(
            '<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 article-col">' +

                '<div class="headers-preloader"><div class="loader"></div></div>' +
                '<div class="img-container">' +
                    $desc_img[0].outerHTML +
                    '<div data-toggle="modal" data-target="#bs-modal" data-rss-title="' +title+ '" data-rss-title-link="' +encodeURIComponent(linkTitle)+ '"><i class="fa fa-external-link" aria-hidden="true"></i></div>' +
                '</div>' +

                '<div class="panel-group">' +
                    '<div class="panel panel-default">' +

                        '<div class="panel-heading">' +
                           '<h4 class="panel-title">' +
                               '<a class="text-link" href="' +linkTitle+ '" target="_blank">' +title+ '</a>' +
                            '</h4>' +
                        '</div>' +

                        '<div id="collapse-NOT-' +nCell+ '" class="panel-collapse collapse in">' +
                            desc_p_HTML +
                        '</div>' +

                        '<div class="panel-footer panel-categories">' +categories_HTML+ '</div>' +

                        '<div class="panel-footer panel-roda-pe">' +
                            '<p>' +pubDate+ '</p>' +
                            '<p>' +
                                '<span class="pull-left">' + author + '</span>' +
                                '<a class="pull-right" href="' +commentsURL+ '" target="_blank"><i class="fa fa-comments" aria-hidden="true"></i></a>' +
                            '</p>' + 
                        '</div>' +

                    '</div>' +
                '</div>' +

            '</div>'
        );

        //Finally we have the DOM re-done.
        //Start loading wheel for each article


        //Last, treat each img as BS responsive:
        $grid.children('div')
            .eq(nCell)
                .find('img')
                    .addClass('img-responsive')
            
        ;
    }

}

/**
 * Draw the HTML of some properties, passed in 'objRSS' grid of the collection of feeds (fetched in 'items' collection ok Objs) from 'arrayFeeds'
 * 
 * @param {Obj} objRSS 
 */
function do_headerItems(objRSS){
    const
        objData  = objRSS.rss.channel
        , header = objData.title + ' v' + objRSS.rss._version
        , descr  = objData.description
        , copy = objData.copyright

    ;
    $('main h1').text(header);
    $('main h2').text(descr);
    $('div.copyright-notice > div').text(copy)
    
}

/**
 * Once a BS Modal is called/open,
 * next draws the HTML of an RSS content, stored already in our 'rssContent' Obj (from a XML feed)
 * 
 * @param {*} evt 
 */
function onShowModal_printArticle(evt){
    const
        $trigger        = $(evt.relatedTarget)                          // Button that triggered the modal; usualy brings us some data tag values, indexes, etc., which is not the case
        , $modal        = $(this)
        , articleToRead = $trigger.closest('div.article-col').html()    //But its an excellent reference to traverse DOM and get the article the trigger is placed on
        //Handy!
        , linkTitle     = $trigger.data('rss-title-link')
        , title         = $trigger.data('rss-title')
        
    ;
    //console.log(articleToRead);

    //Just put the HTML @ Modal:
    $modal.find('div.modal-body')
        .html('').html(articleToRead)

    ;

    //And do some refinements
    //NOTE: we need for BS JS, taking care of this modal, to do all the tasks, measuremnts and adjust it has to do.
    // Tll "then", DOM is not the way you think it is! ;-)
    setTimeout(function(){
        $modal
            .find('div.modal-body')
                .find('.img-container')
                    .find('div[data-toggle="modal"]')
                        .remove()
                        .end()
                    .next('.panel-group')
                        .find('.panel-heading')
                            .remove()
            //Finally:
                            .end()
                        .end()
                    .end()
                .end()
            .find('#rss-reader-title')
                .html(
                    '<a class="text-link" href="' +decodeURIComponent(linkTitle)+ '" target="_blank">' +title+ '</a>'
                )
            .end()

        ;
    }, 500);
                    
}

/**
 * Build an Automated RSS Feed List using just Front End - with JS/jQuery
 * Unfortunately it's not possible anymore... https://developers.google.com/feed/
 * 
 * @param {string} strURL 
 */
function xmlParser(strURL){
    /**
     * Unfortunately, Google Feed API, the only method for not get the x-domain corrs error, is, now, gone for good...
     *      Failed to load https://www.engadget.com/rss-full.xml: No 'Access-Control-Allow-Origin' header is present on the requested resource.
     *      Origin 'http://localhost' is therefore not allowed access.""
     *
     * and about Google API, read it yourself:
     *      https://developers.google.com/feed/
     * 
     * So....the end, @ Front!
     * We could use some PHP to get the info (curl) and pass it to the Front Views.
     * 
     */
    return 'No way José!';

    $.ajax({
        url      : document.location.protocol + '//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=10&callback=?&q=' + encodeURIComponent(strURL),
        dataType : 'json',
        success  : function (data) {
            
            console.log(data);
            //It actually brings the message "This API is no longer available." - try it...
            
            if (data.responseData.feed && data.responseData.feed.entries) {
                $.each(data.responseData.feed.entries, function (i, e) {
                    console.log("------------------------");
                    console.log("title      : " + e.title);
                    console.log("author     : " + e.author);
                    console.log("description: " + e.description);
                });

            } else {
                console.log('ERROOOOORRRRR!');
            }
        }

    });

}