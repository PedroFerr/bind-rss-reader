The purpose of this challenge is to understand how you use HTML/CSS/JavaScript.  
Build a RSS-Reader using this feed (https://www.engadget.com/rss-full.xml)  


## Topics

The resulting webpage must be responsive and the use of Bootstrap is a plus.  
Using the Engadget XML, get the data and build a webpage which lists the latest 10 items.  
Make reasonable assumptions, state your assumptions, and proceed.  