const
    rssContent = {
        "rss": {
            "channel": {
                "title": "Engadget RSS Feed",
                "link": "https://www.engadget.com/rss-full.xml",
                "image": {
                    "url": "https://www.blogsmithmedia.com/www.engadget.com/media/feedlogo.gif?cachebust=true",
                    "title": "Engadget RSS Feed",
                    "link": "https://www.engadget.com/rss-full.xml"
                },
                "language": "en-us",
                "description": "Engadget is a web magazine with obsessive daily coverage of everything new in gadgets and consumer electronics",
                "copyright": "Copyright 2017 AOL Inc. The contents of this feed are available for non-commercial use only.",
                "item": [
                    {
                        "title": {
                            "__cdata": "FCC will help New York investigate fake net neutrality comments"
                        },
                        "link": "https://www.engadget.com/2017/12/04/fcc-to-cooperate-with-new-york-investigation-into-fake-net-neutrality-comments/",
                        "guid": {
                            "_isPermaLink": "false",
                            "__text": "https://www.engadget.com/2017/12/04/fcc-to-cooperate-with-new-york-investigation-into-fake-net-neutrality-comments/"
                        },
                        "comments": "https://www.engadget.com/2017/12/04/fcc-to-cooperate-with-new-york-investigation-into-fake-net-neutrality-comments/#comments",
                        "description": {
                            "__cdata": "\n\t\t\t\t\t\t<img src=\"https://s.aolcdn.com/hss/storage/midas/4baf1c6e7cce1186f2c439b1c4d0dd2a/205919021/fcc-ed.jpg\" /><p>To say the FCC has been reluctant to look into the <a href=\"https://www.engadget.com/2017/11/29/over-half-fcc-net-neutrality-comments-fake/\">millions of fake comments</a> supporting its decision to <a href=\"https://www.engadget.com/2017/11/22/fcc-releases-final-draft-proposal-kill-net-neutrality/\">kill net neutrality</a> would be an understatement. The Commission did nothing to tackle the issue <a href=\"https://www.engadget.com/2017/05/10/anti-net-neutrality-bots-swarm-fcc/\">for months</a>, and <a href=\"https://www.engadget.com/2017/11/21/new-york-blasts-fcc-over-net-neutrality-spam-fight/\">repeatedly stonewalled</a> New York state's investigation. At last, though, there <em>might</em> be some progress. New York Attorney General Eric Schneiderman has <a href=\"https://ag.ny.gov/press-release/ag-schneiderman-announces-fcc-igs-office-reverses-course-after-pressure-signals-intent\">revealed</a> that the FCC Inspector General's office has \"reversed course\" and intends to cooperate with the state's inquiry. Just what that cooperation entails isn't clear, but it beats the virtual silence until now.</p>\n\t\t\t\t\t\t\t<p>Schneiderman and FCC Commissioner Jessica Rosenworcel also joined the 28 Senators calling on the FCC to <a href=\"https://www.engadget.com/2017/12/04/senators-ask-fcc-delay-net-neutrality-vote/\">delay the net neutrality vote</a> until it can conduct a review of the comments. It's \"unacceptable\" that theFCC is planning to scrap net neutrality based on \"corrupted\" comments and an <a href=\"https://www.engadget.com/2017/07/31/fcc-sharing-ddos-details-undermines-security/\">alleged denial of service attack</a>, Rosenworcel said.</p>\n\n<p>While this is a positive sign, we wouldn't count on the FCC providing as much help as New York would like, let alone acting on the investigation or delaying the vote until that investigation is over. Chairman Ajit Pai has made it clear that he intends to gut net neutrality regardless of public objections, and it's doubtful that he'll have a change of heart even if it's clear that most authentic comments reject his plans.</p>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<p><strong>Source: </strong><a target=\"_blank\" href=\"https://ag.ny.gov/press-release/ag-schneiderman-announces-fcc-igs-office-reverses-course-after-pressure-signals-intent\">New York Attorney General</a><!--//--></p>\n\t\t\t\t\t\t\t"
                        },
                        "category": [
                            "astroturfing",
                            "comments",
                            "crime",
                            "fakecomments",
                            "fcc",
                            "gear",
                            "government",
                            "internet",
                            "law",
                            "netneutrality",
                            "newyork",
                            "politics"
                        ],
                        "creator": {
                            "__prefix": "dc",
                            "__cdata": "Jon Fingas"
                        },
                        "pubDate": "Mon, 04 Dec 2017 15:11:00 -0500",
                        "identifier": {
                            "__prefix": "dc",
                            "__text": "21|23296628"
                        }
                    },
                    {
                        "title": {
                            "__cdata": "Import your Instagram contacts to Facebook Messenger"
                        },
                        "link": "https://www.engadget.com/2017/12/04/import-instagram-contacts-facebook-messenger/",
                        "guid": {
                            "_isPermaLink": "false",
                            "__text": "https://www.engadget.com/2017/12/04/import-instagram-contacts-facebook-messenger/"
                        },
                        "comments": "https://www.engadget.com/2017/12/04/import-instagram-contacts-facebook-messenger/#comments",
                        "description": {
                            "__cdata": "\n\t\t\t\t\t\t<img src=\"https://s.aolcdn.com/hss/storage/midas/da3396ee82fb44c9234eedb5a866411c/205918967/insta+on+iPhone+2.jpg\" /><p>Facebook started testing a way to <a href=\"https://www.engadget.com/2017/09/07/instagram-stories-direct-facebook-sharing/\">cross-post</a> Instagram Stories to your Facebook timeline this past September, and finally <a href=\"https://www.engadget.com/2017/10/05/instagram-to-facebook-stories-crosspost/\">released the feature</a> to everyone in early October. You can also <a href=\"https://techcrunch.com/2017/09/20/you-can-now-launch-the-instagram-app-directly-from-facebook/\">launch Instagram</a> directly from the Facebook app, making for an even more integrated experience across the two apps. As initially reported by <a href=\"https://techcrunch.com/2017/12/04/you-can-now-import-your-instagram-contacts-into-messenger/\"><i>TechCrunch</i></a>, Facebook is now testing a feature to import and sync your Instagram contacts with Messenger.</p>\n\t\t\t\t\t\t\t<p>As confirmed in an email from an Instagram spokesperson, it's super easy to connect your two accounts, and completely opt-in. Simply launch Facebook Messenger, tap on the People tab and then hit \"Connect to Instagram.\" You'll then tap a button to connect to your Instagram account, which will find all your mutual Messenger contacts from the photo-sharing service. The app will offer the option to start conversations with those people, or you can skip it and just get back into Messenger proper.</p>\n\n<p><img data-credit=\"Instagram\" src=\"https://o.aolcdn.com/images/dims?crop=1795%2C1023%2C0%2C0&amp;quality=85&amp;format=jpg&amp;resize=1600%2C912&amp;image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2F776216ae8eb5e322848e8844e6c8c0c1%2F205918925%2Finstagram%2Bcontacts.jpeg&amp;client=a1acac3e1b3290917d92&amp;signature=59a568e51683880ed3cdf09f610b09912c89673f\" data-mep=\"2270740\" alt=\"\" data-provider=\"other\" data-provider-asset-id=\"205918925\" /></p>\n\n<p>Allowing the more than <a href=\"https://www.engadget.com/2017/04/26/700-million-people-are-using-instagram/\">700 million people</a> who use Instagram (as of last April) easy access to Messenger is a no brainer for Facebook, as it continues to bring it's two hot social properties together on your phone.</p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Via: </strong><a target=\"_blank\" href=\"https://techcrunch.com/2017/12/04/you-can-now-import-your-instagram-contacts-into-messenger/\">TechCrunch</a><!--//--></p>\n\t\t\t\t\t\t\t\t\t\t"
                        },
                        "category": [
                            "contacts",
                            "facebook",
                            "gear",
                            "import",
                            "instagram",
                            "mobile",
                            "opt-in",
                            "sync"
                        ],
                        "creator": {
                            "__prefix": "dc",
                            "__cdata": "Rob LeFebvre"
                        },
                        "pubDate": "Mon, 04 Dec 2017 14:41:00 -0500",
                        "identifier": {
                            "__prefix": "dc",
                            "__text": "21|23296666"
                        }
                    },
                    {
                        "title": {
                            "__cdata": "Fisker unveils self-driving shuttle built for smart cities"
                        },
                        "link": "https://www.engadget.com/2017/12/04/fisker-unveils-orbit-self-driving-shuttle/",
                        "guid": {
                            "_isPermaLink": "false",
                            "__text": "https://www.engadget.com/2017/12/04/fisker-unveils-orbit-self-driving-shuttle/"
                        },
                        "comments": "https://www.engadget.com/2017/12/04/fisker-unveils-orbit-self-driving-shuttle/#comments",
                        "description": {
                            "__cdata": "\n\t\t\t\t\t\t<img src=\"https://s.aolcdn.com/hss/storage/midas/cf15b1e29cc8fd33dac244b6e6cd574a/205918328/fisker-orbit-shuttle.jpg\" /><p>Believe it or not, Fisker isn't just focused on <a href=\"https://www.engadget.com/2017/08/18/fisker-luxury-ev-debut-january-ces-2018/\">upscale electric cars</a>.  The automaker has <a href=\"https://www.fiskerinc.com/s/FiskerSmartShuttle.pdf\">teamed up</a> with China's Hakim Unique Group on the Orbit, a self-driving electric shuttle tailor-made for <a href=\"https://www.engadget.com/2016/11/03/singapore-smart-nation-smart-city/\">smart cities</a>.  There aren't many details, but it's clearly taking advantage of its driverless nature: the boxy design maximizes passenger space, and there's a huge digital display that tells commuters when the shuttle departs and what its next stop will be.  You wouldn't have to twiddle your thumbs wondering whether or not you'll make it on time.</p>\n\t\t\t\t\t\t\t<p>And unlike <a href=\"https://www.engadget.com/2016/10/04/fisker-teases-an-electric-sports-car-with-a-400-mile-range/\">Fisker's projects to date</a>, you won't be waiting forever to see an Orbit on the road.  The company expects to deliver its first shuttles by the end of 2018.  They'll initially be used as part of a Hakim Unique-implemented smart city.  There's no mention of where this city would be, let alone when it will expand elsewhere.  However, Hakim has a global reach, and it's safe to presume that Fisker doesn't want to confine its autonomous tech to one country.  The biggest challenge is likely to be finding cities and transportation outfits that are willing to incorporate self-driving shuttles -- that requires both the right legal framework and companies that are willing to take a chance on a robotic fleet.</p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Via: </strong><a target=\"_blank\" href=\"https://www.autoblog.com/2017/12/04/fisker-orbit-autonomous-shuttle-ev/\">Autoblog</a><!--//--></p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Source: </strong><a target=\"_blank\" href=\"https://www.fiskerinc.com/s/FiskerSmartShuttle.pdf\">Fisker (PDF)</a><!--//--></p>\n\t\t\t\t\t\t\t"
                        },
                        "category": [
                            "autonomous",
                            "china",
                            "electricvehicle",
                            "ev",
                            "fisker",
                            "gear",
                            "hakimuniquegroup",
                            "masstransit",
                            "publictransportation",
                            "self-driving",
                            "self-drivingcar",
                            "shuttle",
                            "smartcity",
                            "transportation"
                        ],
                        "creator": {
                            "__prefix": "dc",
                            "__cdata": "Jon Fingas"
                        },
                        "pubDate": "Mon, 04 Dec 2017 14:21:00 -0500",
                        "identifier": {
                            "__prefix": "dc",
                            "__text": "21|23296521"
                        }
                    },
                    {
                        "title": {
                            "__cdata": "‘Altered Carbon,’ Netflix’s answer to ‘Blade Runner,’ debuts in February"
                        },
                        "link": "https://www.engadget.com/2017/12/04/netflix-altered-carbon-trailer-blade-runner/",
                        "guid": {
                            "_isPermaLink": "false",
                            "__text": "https://www.engadget.com/2017/12/04/netflix-altered-carbon-trailer-blade-runner/"
                        },
                        "comments": "https://www.engadget.com/2017/12/04/netflix-altered-carbon-trailer-blade-runner/#comments",
                        "description": {
                            "__cdata": "\n\t\t\t\t\t\t<img src=\"https://s.aolcdn.com/hss/storage/midas/c52bb9c9d1889a282439d1c80d6ba49f/205918499/alteredcarbon.jpg\" /><p>Looking for something to sate your <a href=\"https://www.engadget.com/2017/10/20/designing-the-technology-of-blade-runner-2049/\"><em>Blade Runner</em> appetite</a> until this year's <em>2049</em> hits Blu-ray? Then take a look at the first trailer for Netflix's upcoming sci-fi serial <a href=\"https://www.engadget.com/2016/01/20/altered-carbon-new-netflix-series/\"><em>Altered Carbon</em></a>. The quick look has it all: grimy retro-futuristic flying cars, lots of rain, at least one trench coat and a neon-drenched market scene. As far as actual story goes, the series is based on Richard K. Morgan's books of the same name and follows protagonist Takeshi Kovacs as he investigates a murder. The twist? Human consciousness has been digitized and Kovacs was dead for a few centuries prior to being resurrected to take the case. Yeah.</p>\n\t\t\t\t\t\t\t<p>This type of death and resurrection show isn't cheap, and as such, only the well-heeled have access to it. When the deceased in question is the world's richest man, the question of just what \"mortality\" means comes into play. <em>Altered Carbon</em> will debut February 2nd next year.</p>\n\n<center><iframe allow=\"encrypted-media\" allowfullscreen=\"\" frameborder=\"0\" gesture=\"media\" height=\"384\" src=\"https://www.youtube.com/embed/qM7Eob4YFDQ\" width=\"640\"></iframe></center>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<p><strong>Source: </strong><a target=\"_blank\" href=\"https://www.youtube.com/watch?v=qM7Eob4YFDQ&amp;feature=em-uploademail\">Netflix (YouTube)</a><!--//--></p>\n\t\t\t\t\t\t\t"
                        },
                        "category": [
                            "alteredcarbon",
                            "av",
                            "bladerunner",
                            "bladerunner2049",
                            "consciousness",
                            "death",
                            "entertainment",
                            "netflix",
                            "noir",
                            "scifi",
                            "streaming"
                        ],
                        "creator": {
                            "__prefix": "dc",
                            "__cdata": "Timothy J. Seppala"
                        },
                        "pubDate": "Mon, 04 Dec 2017 13:58:00 -0500",
                        "identifier": {
                            "__prefix": "dc",
                            "__text": "21|23296570"
                        }
                    },
                    {
                        "title": {
                            "__cdata": "Select Chromebook owners can get six months of Netflix for free"
                        },
                        "link": "https://www.engadget.com/2017/12/04/google-chromebook-free-netflix/",
                        "guid": {
                            "_isPermaLink": "false",
                            "__text": "https://www.engadget.com/2017/12/04/google-chromebook-free-netflix/"
                        },
                        "comments": "https://www.engadget.com/2017/12/04/google-chromebook-free-netflix/#comments",
                        "description": {
                            "__cdata": "\n\t\t\t\t\t\t<img src=\"https://o.aolcdn.com/images/dims?crop=1400%2C933%2C0%2C0&amp;quality=85&amp;format=jpg&amp;resize=1600%2C1066&amp;image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2Fa2b5296e5a36035b761d2dacc4d5cba8%2F204846863%2Fslide_0001.jpg&amp;client=a1acac3e1b3290917d92&amp;signature=11ed15455d6d9e8169d11afe9749ad27022029d5\" /><p>If you're picking up a Google Pixelbook, a Samsung Chromebook Plus or a Samsung Chromebook Pro this holiday season, Google wants to make your present (whether to yourself or someone else) even better. Through the end of the year, any of these devices comes with six months of Netflix for free. This appears to apply to recent purchases as well.</p>\n\t\t\t\t\t\t\t<p>The deal is for the $10.99 per month plan, which includes streaming to two screens at a time (sorry 4K users). According to the terms, it appears as though if you are already a Netflix subscriber, you can apply the offer to your existing account, even if you have a different plan than the one stated above. Basically, Google is giving users $65.94 in Netflix credit, and you can use it any way you like, except as applied towards gift subscriptions.</p>\n\n<p>To get your free Netflix, you can visit the Chromebook offer site and hit \"Redeem\" from the eligible machine. There are also some other interesting deals to take advantage of, including a $20 Google Play credit for Samsung Plus or Pro owners.</p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Via: </strong><a target=\"_blank\" href=\"https://www.theverge.com/2017/12/4/16733612/google-pixelbook-samsung-chromebook-pro-plus-netflix-free\">The Verge</a><!--//--></p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Source: </strong><a target=\"_blank\" href=\"https://www.google.com/chromebook/offers/\">Google</a><!--//--></p>\n\t\t\t\t\t\t\t"
                        },
                        "category": [
                            "chromebook",
                            "gadgetry",
                            "gadgets",
                            "gear",
                            "google",
                            "googlepixel",
                            "samsung"
                        ],
                        "creator": {
                            "__prefix": "dc",
                            "__cdata": "Swapna Krishna"
                        },
                        "pubDate": "Mon, 04 Dec 2017 13:31:00 -0500",
                        "identifier": {
                            "__prefix": "dc",
                            "__text": "21|23296539"
                        }
                    },
                    {
                        "title": {
                            "__cdata": "‘House of Cards’ returns in 2018 -- without Kevin Spacey"
                        },
                        "link": "https://www.engadget.com/2017/12/04/netflix-house-of-cards-drops-kevin-spacey/",
                        "guid": {
                            "_isPermaLink": "false",
                            "__text": "https://www.engadget.com/2017/12/04/netflix-house-of-cards-drops-kevin-spacey/"
                        },
                        "comments": "https://www.engadget.com/2017/12/04/netflix-house-of-cards-drops-kevin-spacey/#comments",
                        "description": {
                            "__cdata": "\n\t\t\t\t\t\t<img src=\"https://o.aolcdn.com/images/dims?crop=3500%2C2517%2C0%2C0&amp;quality=85&amp;format=jpg&amp;resize=1600%2C1151&amp;image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2Fc8068d0f20bf88876688f7a0f704fc76%2F204470131%2FRTX18SIC.jpeg&amp;client=a1acac3e1b3290917d92&amp;signature=7da71adaec97e007e0c71e9838827cdcb22e4649\" /><p>Today, Netflix announced that it has reached an agreement to resume production on the sixth season of its tentpole show <em>House of Cards</em>. <em><a href=\"http://variety.com/2017/digital/news/house-of-cards-season-6-production-2018-kevin-spacey-1202629619/\">Variety</a> </em>reports that the final season will consist of eight episodes starring Robin Wright. Kevin Spacey will not appear in the season. Production on the show will resume in early 2018.</p>\n\t\t\t\t\t\t\t<p>In October, <a href=\"https://www.engadget.com/2017/10/30/netfilx-to-end-house-of-cards-on-season-6/\">Netflix canceled <em>House of Cards</em></a> following actor Anthony Rapp's sexual assault allegations against its star, Kevin Spacey. The very next day, the streaming service <a href=\"https://www.engadget.com/2017/10/31/netflix-suspends-house-of-cards-production-amid-spacey-allegat/\">suspended production on the sixth and final season of the show</a> after members of the <em>House of Cards</em> crew accused Spacey of harassment and assault on set. The service made it clear that <a href=\"https://www.engadget.com/2017/11/03/netflix-kevin-spacey-house-of-cards/\">those involved would not move forward with the show</a> in any way as long as Kevin Spacey was attached. Now, it looks like the company has solved that problem. Rumors say that the show's storyline will incorporate the <a href=\"http://variety.com/2017/tv/news/house-of-cards-kevin-spacey-1202606647/\">death of Kevin Spacey's character</a>.</p>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<p><strong>Source: </strong><a target=\"_blank\" href=\"http://variety.com/2017/digital/news/house-of-cards-season-6-production-2018-kevin-spacey-1202629619/\">Variety</a><!--//--></p>\n\t\t\t\t\t\t\t"
                        },
                        "category": [
                            "av",
                            "entertainment",
                            "houseofcards",
                            "netflix"
                        ],
                        "creator": {
                            "__prefix": "dc",
                            "__cdata": "Swapna Krishna"
                        },
                        "pubDate": "Mon, 04 Dec 2017 13:12:00 -0500",
                        "identifier": {
                            "__prefix": "dc",
                            "__text": "21|23296584"
                        }
                    },
                    {
                        "title": {
                            "__cdata": "LiDAR strips landscapes down to their bare glory"
                        },
                        "link": "https://www.engadget.com/2017/12/04/lidar-bare-earth-landscapes-the-big-picture/",
                        "guid": {
                            "_isPermaLink": "false",
                            "__text": "https://www.engadget.com/2017/12/04/lidar-bare-earth-landscapes-the-big-picture/"
                        },
                        "comments": "https://www.engadget.com/2017/12/04/lidar-bare-earth-landscapes-the-big-picture/#comments",
                        "description": {
                            "__cdata": "\n\t\t\t\t\t\t<img src=\"https://s.aolcdn.com/hss/storage/midas/33c327df85ff7772723605b5f1db5eaa/205916178/big-picture-washington-state-lidar-2017-12-03-01.jpg\" /><p>LiDAR is having a moment right now helping <a href=\"https://www.engadget.com/2017/11/29/velodyne-lidar-helps-self-driving-cars-survive-the-highway/\">self-driving cars</a> and <a href=\"https://www.engadget.com/2017/09/13/panasonics-lidar-sensor-will-stop-embarrassing-robot-falls/\">robots</a> not hit things, but don't forget about what else it can do. In a study called <a href=\"https://wadnr.maps.arcgis.com/apps/Cascade/index.html?appid=36b4887370d141fcbb35392f996c82d9\">The Bare Earth</a>, scientists from the Washington Geological Survey used it to image the ground right down to dirt and rocks. Stripped of trees and other distractions, the images provide not only valuable geological survey data, but stunning, otherworldly views of our planet.</p>\n\t\t\t\t\t\t\t<p>The image above depicts a LiDAR relative elevation model (REM), showing current and previous channels carved out by the Sauk River in Washington State's Skagit and Snohomish counties. In the regular satellite image below, however, only the active, vegetation-free channels are clearly visible -- a striking display of what the technique can reveal.</p>\n\n<p style=\"text-align: center;\"><img data-credit=\"Washington Geological Survey\" src=\"https://s.aolcdn.com/hss/storage/midas/b492b112006cf07c65fe1ddf191cfe9f/205918162/big-picture-lidar-washington-2017-12-03-02.jpg\" data-mep=\"2270410\" alt=\"\" data-provider=\"other\" data-provider-asset-id=\"205918162\" /></p>\n\n<p>\"REMs are extremely useful in discerning where river channels have migrated in the past by vividly displaying fluvial features such as meander scars, terraces, and oxbow lakes,\" explains Washington State geologist Daniel E. Coe <a href=\"https://www.dnr.wa.gov/publications/ger_presentations_dmt_2016_coe.pdf\">in a PDF</a>. \"This type of information is very informative in channel migration and flood studies, as well as a host of other engineering and habitat assessments.\"</p>\n\n<p>Carried on special aircraft, LiDAR produces visible-light laser beams. While much of it bounces off of trees and vegetation, enough make it through to the ground. By examining the raw data to see which beams travel the farthest, scientists can \"edit out\" trees, vegetation and man-made structures. That reveals hidden seismic faults, glacial landforms, landslides, lava flows and other features that are invisible on a regular satellite photo.</p>\n\n<p>Used in this way, the images also become a fantastic educational tool. Understanding things like drumlins, moraines and kettles can be pretty tough, but seeing a landform stripped to the bare ground brings the concepts to life. Suddenly, it becomes much easier to visualize how glaciers, lava flow (above), tsunamis and other natural phenomena have scarred (and will scar) our planet's surface over time.</p>\n\n<p>Considering that they're made by scientific instruments, the LiDAR images also happen to be beautiful (seriously, <a href=\"https://wadnr.maps.arcgis.com/apps/Cascade/index.html?appid=36b4887370d141fcbb35392f996c82d9\">check them out</a>). You could even say that like other types of art, they strip away the surface to show things we may not want to see. A great example is the <a href=\"http://s3-us-west-2.amazonaws.com/dger/Photos/lidar/ger_lidar_toe_jam_hill_fault_composite.png\">Toe Jam Hill fault scarp</a> (part of the Seattle fault zone), first revealed by a <a href=\"https://geomaps.wr.usgs.gov/pacnw/resfzplr1.html\">LiDAR scan in the '90s</a>.</p>\n\n<p>Subsequent trenching showed that it was the site of a single large earthquake 1,100 years ago, part of the great Seattle subduction quake at around 900 AD. That seismic event has become a part of the <a href=\"https://www.researchgate.net/profile/John_Clague/publication/249551808_Folklore_and_earthquakes_Native_American_oral_traditions_from_Cascadia_compared_with_written_traditions_from_Japan/links/57560f6f08ae155a87b9cf08/Folklore-and-earthquakes-Native-American-oral-traditions-from-Cascadia-compared-with-written-traditions-from-Japan.pdf\">oral history</a> and legend of Coast Salish Native Americans in the region, and is an <a href=\"https://www.livescience.com/21289-seattle-fault-earthquake-threat.html\">ongoing concern</a> to folks in Washington's Puget Sound area. LiDAR has become an important tool in confirming ancient stories, while helping us learn about future ones -- hopefully before they happen.</p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Via: </strong><a target=\"_blank\" href=\"https://kottke.org/17/12/whats-under-the-trees-lidar-exposes-the-hidden-landscapes-of-forested-areas\">Kottke</a><!--//--></p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Source: </strong><a target=\"_blank\" href=\"https://wadnr.maps.arcgis.com/apps/Cascade/index.html?appid=36b4887370d141fcbb35392f996c82d9\">Washington State Geological Survey</a><!--//--></p>\n\t\t\t\t\t\t\t"
                        },
                        "category": [
                            "bareearth",
                            "cameras",
                            "channels",
                            "gear",
                            "geologicalsurvey",
                            "geology",
                            "lidar",
                            "maps",
                            "saulkriver",
                            "thebigpicture",
                            "thenewbigpicture",
                            "washingtonstate"
                        ],
                        "creator": {
                            "__prefix": "dc",
                            "__cdata": "Steve Dent"
                        },
                        "pubDate": "Mon, 04 Dec 2017 13:00:00 -0500",
                        "identifier": {
                            "__prefix": "dc",
                            "__text": "21|23295671"
                        }
                    },
                    {
                        "title": {
                            "__cdata": "Senators ask the FCC to delay its net neutrality vote (updated)"
                        },
                        "link": "https://www.engadget.com/2017/12/04/senators-ask-fcc-delay-net-neutrality-vote/",
                        "guid": {
                            "_isPermaLink": "false",
                            "__text": "https://www.engadget.com/2017/12/04/senators-ask-fcc-delay-net-neutrality-vote/"
                        },
                        "comments": "https://www.engadget.com/2017/12/04/senators-ask-fcc-delay-net-neutrality-vote/#comments",
                        "description": {
                            "__cdata": "\n\t\t\t\t\t\t<img src=\"https://s.aolcdn.com/hss/storage/midas/ada5bdd630dd8feaa337919ccd9d5a36/205918677/ajit-ed.jpg\" /><p>A group of senators has sent a <a href=\"https://www.hassan.senate.gov/imo/media/doc/171204.Pai.Ltr.NN.Bots.pdf\">letter</a> to the FCC asking the commission to delay its December 14th vote on proposed <a href=\"https://www.engadget.com/2017/12/01/net-neutrality-faq-title-i-title-ii-2017/\">net neutrality protection rollbacks</a>, <em><a href=\"http://thehill.com/policy/technology/363110-group-of-senators-calls-on-fcc-to-delay-net-neutrality-vote\">The Hill</a></em> reports. Led by Senator Maggie Hassan, 28 senators signed the letter, which pointed to evidence that the proposal's public comments were rife with fraudulent posts. \"A free and open internet is vital to ensuring a level playing field online, and we believe that your proposed action may be based on an incomplete understanding of the public record in this proceeding,\" they wrote. \"In fact, there is good reason to believe that the record may be replete with fake or fraudulent comments, suggesting that your proposal is fundamentally flawed.\"</p>\n\t\t\t\t\t\t\t<p>A number of groups have found evidence that many of the 22 million public comments on the FCC proposal might be fake. Data scientist <a href=\"https://www.engadget.com/2017/11/25/over-1-3-million-anti-net-neutrality-fcc-comments-are-fakes/\">Jeff Kao's report</a> noted that at least 1.3 million comments were fake and came from a central source while the <a href=\"https://www.engadget.com/2017/11/29/over-half-fcc-net-neutrality-comments-fake/\">Pew Research Center</a> found that over half of the comments came from temporary, duplicate or fake email addresses. Further, in light of evidence that some comments used other people's identities without their consent, New York State Attorney General Eric Schneiderman has <a href=\"https://www.engadget.com/2017/11/21/new-york-blasts-fcc-over-net-neutrality-spam-fight/\">sent a letter to the FCC</a> chastising the commission for not looking into the fraudulent commentary and <a href=\"https://www.engadget.com/2017/11/29/new-yorkers-report-identities-fake-net-neutrality/\">set up a portal</a> so New Yorkers can see if their identities were falsely used during the public comment period.</p>\n\n<p>The letter, signed by Chuck Schumer, Bernie Sanders, Elizabeth Warren and Cory Booker, requests that the FCC delay its vote until it can thoroughly review the public record and prove its accuracy. \"The FCC must invest its time and resources into obtaining a more accurate picture of the record as understanding of that record is essential to reaching a defensible resolution to this proceeding,\" they wrote.</p>\n\n<p><strong>Update: </strong>Chairman Pai's office <a href=\"https://arstechnica.com/tech-policy/2017/12/fcc-chair-refuses-to-delay-net-neutrality-vote-despite-pending-court-case/\">tells </a><em><a href=\"https://arstechnica.com/tech-policy/2017/12/fcc-chair-refuses-to-delay-net-neutrality-vote-despite-pending-court-case/\">Ars Technica</a> </em>the vote will go on as scheduled on December 14th. The statement explains that net neutrality supporters \"are becoming more desperate by the day\" as the push to block Pai's plan \"has stalled.\"</p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Via: </strong><a target=\"_blank\" href=\"http://thehill.com/policy/technology/363110-group-of-senators-calls-on-fcc-to-delay-net-neutrality-vote\">The Hill</a><!--//--></p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Source: </strong><a target=\"_blank\" href=\"https://www.hassan.senate.gov/imo/media/doc/171204.Pai.Ltr.NN.Bots.pdf\">Senator Hassan</a><!--//--></p>\n\t\t\t\t\t\t\t"
                        },
                        "category": [
                            "ajitpai",
                            "berniesanders",
                            "chuckschumer",
                            "corybooker",
                            "elizabethwarren",
                            "ericschneiderman",
                            "fcc",
                            "fraud",
                            "gear",
                            "internet",
                            "netneutrality",
                            "pewresearchcenter",
                            "politics",
                            "publiccomment",
                            "senator"
                        ],
                        "creator": {
                            "__prefix": "dc",
                            "__cdata": "Mallory Locklear"
                        },
                        "pubDate": "Mon, 04 Dec 2017 12:44:00 -0500",
                        "identifier": {
                            "__prefix": "dc",
                            "__text": "21|23296540"
                        }
                    },
                    {
                        "title": {
                            "__cdata": "Spacesuit 'take me home' feature could save lost astronauts"
                        },
                        "link": "https://www.engadget.com/2017/12/04/spacesuit-take-me-home-feature/",
                        "guid": {
                            "_isPermaLink": "false",
                            "__text": "https://www.engadget.com/2017/12/04/spacesuit-take-me-home-feature/"
                        },
                        "comments": "https://www.engadget.com/2017/12/04/spacesuit-take-me-home-feature/#comments",
                        "description": {
                            "__cdata": "\n\t\t\t\t\t\t<img src=\"https://o.aolcdn.com/images/dims?crop=1851%2C1231%2C0%2C0&amp;quality=85&amp;format=jpg&amp;resize=1600%2C1064&amp;image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2Fbe16f2b5af866dfb1b6f86e4683b66b6%2F205918061%2FRTXF7RD.jpeg&amp;client=a1acac3e1b3290917d92&amp;signature=3c856fc6e8da07150a53f4aa4110884814bc8157\" /><p>The greatest fear for many astronauts is to get lost or disoriented <a href=\"https://www.engadget.com/2017/11/06/nasa-argos-spacewalk-simulator-big-picture/\">during a spacewalk</a>, especially if it's untethered.  How do you get back to safety with no sense of direction, little to no help and a limited supply of oxygen?  Researchers at Draper might offer a lifeline.  They recently <a href=\"http://www.draper.com/news/spacesuit-comes-take-me-home-button\">applied</a> for a patent on a self-return feature in spacesuits that would automatically navigate back to the astronaut's home ship.  A spacefarer in a panic could just slap a button and know they would get back to the airlock.</p>\n\t\t\t\t\t\t\t<p>The trick is to equip the suit with sensors that track motion and position relative to that of a relatively stationary object like a spacecraft, with alternative methods if one system or another doesn't work.  Since GPS isn't exactly viable in space, it could use star tracking or vision-boosted navigation to get bearings.  Draper is hoping for an autonomous system that would trigger thrusters all on its own, but it's open to the possibility of a manual system that uses an in-visor display and sensory cues to guide astronauts homeward.</p>\n\n<p>This is only a patent, and it isn't guaranteed to go into space any time soon.  However, NASA has been backing Draper's research -- it's interested in advancing spacesuit design.  And as Draper notes, the basic ideas behind this could still be helpful for anyone in a suit who needs an urgent trip home, whether it's a deep sea diver or a firefighter <a href=\"https://www.engadget.com/2017/10/13/future-of-firefighting/\">in the middle of a burning building</a>.</p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Via: </strong><a target=\"_blank\" href=\"https://techcrunch.com/2017/12/04/new-spacesuit-go-home-function-could-save-astronaut-lives/\">TechCrunch</a><!--//--></p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Source: </strong><a target=\"_blank\" href=\"http://www.draper.com/news/spacesuit-comes-take-me-home-button\">Draper</a><!--//-->, <a target=\"_blank\" href=\"https://patents.google.com/patent/US20170192425A1/en?inventor=kevin+r+duda\">Google Patents</a><!--//--></p>\n\t\t\t\t\t\t\t"
                        },
                        "category": [
                            "astronaut",
                            "draper",
                            "gear",
                            "nasa",
                            "patent",
                            "space",
                            "spacesuit"
                        ],
                        "creator": {
                            "__prefix": "dc",
                            "__cdata": "Jon Fingas"
                        },
                        "pubDate": "Mon, 04 Dec 2017 12:19:00 -0500",
                        "identifier": {
                            "__prefix": "dc",
                            "__text": "21|23296426"
                        }
                    },
                    {
                        "title": {
                            "__cdata": "The best VR headsets and games to give as gifts"
                        },
                        "link": "https://www.engadget.com/2017/12/04/engadget-holiday-gift-guide-vr-headsets-games/",
                        "guid": {
                            "_isPermaLink": "false",
                            "__text": "https://www.engadget.com/2017/12/04/engadget-holiday-gift-guide-vr-headsets-games/"
                        },
                        "comments": "https://www.engadget.com/2017/12/04/engadget-holiday-gift-guide-vr-headsets-games/#comments",
                        "description": {
                            "__cdata": "\n\t\t\t\t\t\t<img src=\"https://s.aolcdn.com/hss/storage/midas/ef99736ff72c00235ef0a50e14ab5c43/205882443/vr-lede-2.jpg\" /><p>Virtual reality headsets were once so expensive that they would have only made suitable gifts for early adopters and serious gamers. By now, though, prices have dropped across the board, and there are enough compatible games that we're willing to recommend these headsets to a broader audience. The VR section of our <a href=\"https://www.engadget.com/holiday-gift-guide-2017/\">holiday gift guide</a> includes items at both the budget end of the spectrum (think: <a href=\"https://www.engadget.com/2017/08/23/samsung-new-gear-vr-galaxy-note-8/\">Samsung's Gear VR</a> and Google's <a href=\"https://www.engadget.com/2017/10/17/daydream-view-vr-headset-2017-review/\">refreshed Daydream View</a>) alongside higher-end offerings like the <a href=\"https://www.amazon.com/gp/product/B01N36E1XT/ref=as_li_tl?ie=UTF8&amp;tag=engadget00-20&amp;camp=1789&amp;creative=9325&amp;linkCode=as2&amp;creativeASIN=B01N36E1XT&amp;linkId=8709162917911f086fc80c2586ddb922\">PlayStation VR</a> and <a href=\"https://www.engadget.com/2017/07/14/oculus-rift-and-touch-bundle-499/\">Oculus Rift and Touch bundle</a>. Need some games to go with it? We suggest <a href=\"https://www.amazon.com/gp/product/B01GW8Y17G/ref=as_li_tl?ie=UTF8&amp;tag=engadget00-20&amp;camp=1789&amp;creative=9325&amp;linkCode=as2&amp;creativeASIN=B01GW8Y17G&amp;linkId=ee35b0d4519c735b9f2b98edd860155c&amp;th=1\"><em>Resident Evil 7: Biohazard</em></a>, <a href=\"https://www.engadget.com/2017/08/23/fallout-doom-skyrim-vr-this-year/\"><em>Fallout 4 VR</em></a>, <a href=\"https://www.engadget.com/2017/05/26/superhot-vr-htc-vive-launch/\"><em>Superhot VR</em></a> and <em><a href=\"https://www.engadget.com/2017/08/09/rez-infinite-pc-vr/\">Rez Infinite</a></em>. Need some accessories for someone who already owns a headset? Try the <a href=\"https://www.amazon.com/gp/product/B06XGPF3CW/ref=as_li_tl?ie=UTF8&amp;tag=engadget00-20&amp;camp=1789&amp;creative=9325&amp;linkCode=as2&amp;creativeASIN=B06XGPF3CW&amp;linkId=44301da465d4993a26eeff4068dd8c7a\">PSVR Aim Controller <em>Farpoint</em> bundle</a> or the <a href=\"https://www.amazon.com/gp/product/B06Y2GDXMC/ref=as_li_tl?ie=UTF8&amp;tag=engadget00-20&amp;camp=1789&amp;creative=9325&amp;linkCode=as2&amp;creativeASIN=B06Y2GDXMC&amp;linkId=02a8c83cf040cb0380c53d10f76a4214\">HTC Vive Deluxe Audio Strap</a>.</p>\n\n<p></p>\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t\t\t\t\t<p><strong>Source: </strong><a target=\"_blank\" href=\"https://www.engadget.com/holiday-gift-guide-2017/\">Engadget Holiday Gift Guide 2017</a><!--//--></p>\n\t\t\t\t\t\t\t"
                        },
                        "category": [
                            "av",
                            "bethesda",
                            "capcom",
                            "daydreamview",
                            "enhancegames",
                            "facebook",
                            "fallout4vr",
                            "gaming",
                            "gear",
                            "gearvr2017",
                            "google",
                            "greenmangaming",
                            "hgg2017",
                            "htc",
                            "htcvive",
                            "oculus",
                            "oculusrift",
                            "oculustouch",
                            "playstationvr",
                            "psvr",
                            "psvraimcontroller",
                            "residentevil7",
                            "residentevil7biohazard",
                            "rezinfinite",
                            "samsung",
                            "sony",
                            "superhotvr",
                            "video",
                            "vivedeluxeaudiostrap"
                        ],
                        "creator": {
                            "__prefix": "dc",
                            "__cdata": "Engadget"
                        },
                        "pubDate": "Mon, 04 Dec 2017 12:00:00 -0500",
                        "identifier": {
                            "__prefix": "dc",
                            "__text": "21|23284356"
                        }
                    },
                    {
                        "title": {
                            "__cdata": "SEC Cyber Unit's first charges target cryptocurrency fraud"
                        },
                        "link": "https://www.engadget.com/2017/12/04/sec-cyber-units-first-charges-cryptocurrency-fraud/",
                        "guid": {
                            "_isPermaLink": "false",
                            "__text": "https://www.engadget.com/2017/12/04/sec-cyber-units-first-charges-cryptocurrency-fraud/"
                        },
                        "comments": "https://www.engadget.com/2017/12/04/sec-cyber-units-first-charges-cryptocurrency-fraud/#comments",
                        "description": {
                            "__cdata": "\n\t\t\t\t\t\t<img src=\"https://o.aolcdn.com/images/dims?crop=3400%2C2266%2C0%2C0&amp;quality=85&amp;format=jpg&amp;resize=1600%2C1066&amp;image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2F903cac42fbce63899545ae63fa7917d5%2F205918077%2Fthe-us-securities-and-exchange-commission-seal-is-displayed-outside-picture-id130425838&amp;client=a1acac3e1b3290917d92&amp;signature=9d0eab3fa87df0b4d4ea7c057cc251ac341bbe50\" /><p>The <a href=\"https://www.engadget.com/2017/03/10/sec-rejects-winklevoss-plan-trade-bitcoin-stock/\">Securities and Exchange Commission's</a> new Cyber Unit has filed its first charges since <a href=\"https://www.engadget.com/2017/09/26/sec-cyber-unit/\">being formed in September</a>. The unit's case is being brought against a company called PlexCorps, its founder Dominic Lacroix and his partner Sabrina Paradis-Royer and the <a href=\"https://www.engadget.com/2017/09/21/sec-hack-insider-trading/\">SEC</a> claims that Lacroix and Paradis-Royer were actively defrauding investors. PlexCorps was engaged in an initial coin offering (ICO) -- which was selling securities called PlexCoin -- that had already raised around $15 million since August and it was fraudulently promising that investors would see a 13-fold profit in just under one month. The SEC obtained an emergency asset freeze to halt the ICO.</p>\n\t\t\t\t\t\t\t<p>The SEC's charges seek permanent injunctions, a release of all funds collected so far as well as interest and penalties. In a <a href=\"https://www.sec.gov/news/press-release/2017-219\">statement</a>, Robert Cohen, head of the Cyber Unit, said, \"This first Cyber Unit case hits all of the characteristics of a full-fledged cyber scam and is exactly the kind of misconduct the unit will be pursuing. We acted quickly to protect retail investors from this initial coin offering's false promises.\"</p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Via: </strong><a target=\"_blank\" href=\"https://www.reuters.com/article/us-usa-sec-cyberfraud/secs-cyber-unit-files-first-charges-alleges-fraud-scheme-idUSKBN1DY1SN?feedType=RSS&amp;feedName=technologyNews&amp;utm_source=Twitter&amp;utm_medium=Social&amp;utm_campaign=Feed%3A+reuters%2FtechnologyNews+%28Reuters+Technology+News%29\">Reuters</a><!--//--></p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Source: </strong><a target=\"_blank\" href=\"https://www.sec.gov/news/press-release/2017-219\">SEC</a><!--//--></p>\n\t\t\t\t\t\t\t"
                        },
                        "category": [
                            "business",
                            "cryptocurrency",
                            "cybersecurity",
                            "cyberunit",
                            "fraud",
                            "gear",
                            "initialcoinoffering",
                            "internet",
                            "securities",
                            "securitiesandexchangecommission",
                            "security"
                        ],
                        "creator": {
                            "__prefix": "dc",
                            "__cdata": "Mallory Locklear"
                        },
                        "pubDate": "Mon, 04 Dec 2017 11:57:00 -0500",
                        "identifier": {
                            "__prefix": "dc",
                            "__text": "21|23296450"
                        }
                    },
                    {
                        "title": {
                            "__cdata": "UK plans crackdown on criminals using Bitcoin to launder money"
                        },
                        "link": "https://www.engadget.com/2017/12/04/uk-treasury-eu-bitcoin-regulation-crime/",
                        "guid": {
                            "_isPermaLink": "false",
                            "__text": "https://www.engadget.com/2017/12/04/uk-treasury-eu-bitcoin-regulation-crime/"
                        },
                        "comments": "https://www.engadget.com/2017/12/04/uk-treasury-eu-bitcoin-regulation-crime/#comments",
                        "description": {
                            "__cdata": "\n\t\t\t\t\t\t<img src=\"https://o.aolcdn.com/images/dims?crop=5472%2C3648%2C0%2C0&amp;quality=85&amp;format=jpg&amp;resize=1600%2C1067&amp;image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2F131ded23976a90ba93d953e03f19ce53%2F205894809%2FRTS1J7XZ.jpeg&amp;client=a1acac3e1b3290917d92&amp;signature=f3a57317f557a740b95a10c0c31c917b28dd972b\" /><p>The UK government wants to increase regulation around Bitcoin by expanding financial regulations imposed by the European Union. It follows growing concerns that the cryptocurrency is being used to facilitate crime, including drug dealers, brothels and gangs. Stephen Barclay, economic secretary for HM Treasury, <a href=\"https://www.parliament.uk/business/publications/written-questions-answers-statements/written-question/Commons/2017-10-27/110111/\">revealed in a notice</a> that British legislators were negotiating amendments to the EU-wide 4th Anti-Money Laundering Directive. It would bring Bitcoin exchange and wallet providers under the purview of relevant national authorities, forcing them to carry out due diligence on customers and report suspicious activity. Traders would also be required to disclose their identities, <a href=\"https://www.theguardian.com/technology/2017/dec/04/bitcoin-uk-eu-plan-cryptocurrency-price-traders-anonymity\">according to <em>The Guardian</em></a>.<br />\n<br />\n\"We are working to address concerns about the use of cryptocurrencies, by negotiating to bring virtual currency exchange platforms and some wallet providers within Anti-Money Laundering and Counter-Terrorist Financing regulation,\" a Treasury spokesperson told Engadget.</p>\n\n<p>All of this would be a huge shake-up in the cryptocurrency community. At the moment, it's possible to remain anonymous while dealing in Bitcoin, Ethereum, and similar digital money. That freedom is part of the appeal, of course, alongside their distributed ledgers and decentralised structures. The flip-side is that Bitcoin now has a reputation for fuelling all sorts of criminal activity. As <a href=\"http://uk.businessinsider.com/drug-dealers-laundering-their-money-at-bitcoin-atms-london-police-say-2017-12?r=US&amp;IR=T\"><em>Business Insider</em> reports</a>, the Metropolitan Police recently held a crime briefing to discus the problem. It said Bitcoin ATMs are increasingly being used to deposit cash without alerting its officers.</p>\n\n<p>\"If you move large quantities of cash around it leaves you vulnerable to other criminals,\" detective chief superintendent Michael Gallagher, head of the Met's Serious and Organised Crime Command added. \"It's in their own interest, in terms of protection, to use this.\"</p>\n\n<p>The UK expects its negotiations with the EU to end in the next few months. If they're successful, the British government will need to create or amend domestic legislation to enforce the new rules. In the meantime, Bitcoin users are free to operate as normal. The currency's value currently sits at more than $11,200, a record high. It is, therefore, no surprise to see more regulators looking twice at Bitcoin's impact on the international economy.</p>\n\n<p>John Mann, a member of the Treasury select committee, <a href=\"http://www.telegraph.co.uk/news/2017/12/03/bitcoin-crackdown-amid-fears-money-laundering-tax-dodging/\">told <em>the Telegraph</em></a> that his cross-party group was likely to hold an enquiry next year. \"It would be timely to have a proper look at what this means,\" he said. \"It may be that we want to speed up our use of these kinds of things in this country, but that makes it all the more important that we don't have a regulatory lag.\"</p>\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t\t\t\t\t<p><strong>Source: </strong><a target=\"_blank\" href=\"https://www.parliament.uk/business/publications/written-questions-answers-statements/written-question/Commons/2017-10-27/110111/\">Parliament.UK</a><!--//--></p>\n\t\t\t\t\t\t\t"
                        },
                        "category": [
                            "bitcoin",
                            "cryptocurrency",
                            "politics",
                            "tomorrow"
                        ],
                        "creator": {
                            "__prefix": "dc",
                            "__cdata": "Nick Summers"
                        },
                        "pubDate": "Mon, 04 Dec 2017 11:45:00 -0500",
                        "identifier": {
                            "__prefix": "dc",
                            "__text": "21|23296406"
                        }
                    },
                    {
                        "title": {
                            "__cdata": "Life may be easier to find on planets outside the ‘habitable zone’ "
                        },
                        "link": "https://www.engadget.com/2017/12/04/life-on-icy-worlds-with-subsurface-oceans/",
                        "guid": {
                            "_isPermaLink": "false",
                            "__text": "https://www.engadget.com/2017/12/04/life-on-icy-worlds-with-subsurface-oceans/"
                        },
                        "comments": "https://www.engadget.com/2017/12/04/life-on-icy-worlds-with-subsurface-oceans/#comments",
                        "description": {
                            "__cdata": "\n\t\t\t\t\t\t<img src=\"https://o.aolcdn.com/images/dims?crop=5616%2C3742%2C0%2C0&amp;quality=85&amp;format=jpg&amp;resize=1600%2C1066&amp;image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2F5ee4e35ce54940bef1192d0569cff7ad%2F204818442%2F577158213.jpg&amp;client=a1acac3e1b3290917d92&amp;signature=d544d849f41c982b80319c629d817a0e5c5a9676\" /><p>When scientists are looking for worlds that might harbor life on other planets, they tend to look for worlds that have features similar to Earth's. It makes sense; after all, our dominant theories on how life evolved on the planet center on the presence of oxygen, organic molecules and liquid water. But now, two scientists are positing that we may be severely limiting ourselves by only looking for rocky planets with surface oceans. A study published last week at <a href=\"https://arxiv.org/pdf/1711.09908.pdf\">arXiv.org</a> outlines the possibility that it's more likely that scientists will find life on icy worlds with subsurface oceans.</p>\n\t\t\t\t\t\t\t<p>The issue here is the definition of the \"<a href=\"https://www.engadget.com/2017/07/13/unlikely-that-trappist-1-planets-support-life/\">habitable zone</a>\" of a star. Right now, scientists think that they are most likely to find life on other planets within the habitable zone of a star, and that definition is based on the Earth. Scientists are looking primarily at Earth-like planets (rocky, with surface liquid water) to find life. But Manasvi Lingam and Abraham Loeb posit that this is too narrow a search. In their study, they look closely at the concept of habitable zone and how it's not necessarily a predictor for habitability. For example, Mars and Venus are within the sun's habitable zone.</p>\n\n<p>However, planets outside this area are capable of supporting liquid oceans underneath a crust of ice. We see this in our own solar system, thanks to worlds such as Europa, Titan and <a href=\"https://www.engadget.com/2016/11/18/researchers-offer-new-evidence-of-a-liquid-ocean-on-pluto/\">possibly even Pluto</a>. Lingam and Loeb calculate the advantages and disadvantages life would have on these planets, as well as the likelihood life could even exist on them.</p>\n\n<p>They conclude that these types of icy planets with liquid oceans could exist across a wide range of conditions, and that they are much more common (around 1,000 times) than rocky planets within a star's habitable zone. \"As these worlds are likely to be far more abundant than the standard paradigm of rocky planets in the HZ of stars, we suggest that more effort should focus on modeling and understanding the prospects for life in subsurface oceans,\" the study says. After all, life may face more challenges on these worlds than on an Earth-like planet, but if it's just a numbers game, chances are that at least a few of them will support life.</p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Via: </strong><a target=\"_blank\" href=\"https://www.universetoday.com/137995/hundreds-icy-worlds-life-rocky-planets-galaxy/\">Universe Today</a><!--//--></p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Source: </strong><a target=\"_blank\" href=\"https://arxiv.org/pdf/1711.09908.pdf\">arXiv.org</a><!--//--></p>\n\t\t\t\t\t\t\t"
                        },
                        "category": [
                            "exoplanets",
                            "extraterrestirallife",
                            "habitablezone",
                            "space",
                            "tomorrow"
                        ],
                        "creator": {
                            "__prefix": "dc",
                            "__cdata": "Swapna Krishna"
                        },
                        "pubDate": "Mon, 04 Dec 2017 11:33:00 -0500",
                        "identifier": {
                            "__prefix": "dc",
                            "__text": "21|23296379"
                        }
                    },
                    {
                        "title": {
                            "__cdata": "How to stream a paid UFC fight: pretend it’s a video game"
                        },
                        "link": "https://www.engadget.com/2017/12/04/ufc-218-twitch-stream/",
                        "guid": {
                            "_isPermaLink": "false",
                            "__text": "https://www.engadget.com/2017/12/04/ufc-218-twitch-stream/"
                        },
                        "comments": "https://www.engadget.com/2017/12/04/ufc-218-twitch-stream/#comments",
                        "description": {
                            "__cdata": "\n\t\t\t\t\t\t<img src=\"https://o.aolcdn.com/images/dims?crop=3098%2C2214%2C0%2C0&amp;quality=85&amp;format=jpg&amp;resize=1600%2C1143&amp;image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2F200c62663d37253d05d9046bd3ba1b5c%2F205917562%2Fmax-holloway-punches-jose-aldo-of-brazil-in-their-ufc-featherweight-picture-id884622948&amp;client=a1acac3e1b3290917d92&amp;signature=fe54294dae0c3520b224bee6bbfddc0e34a0ddbe\" /><p>Here's a fun one to start your week: On Saturday night, a Twitch streamer broadcasted the entirety of UFC 218 and, to skirt DMCA takedowns, he pantomimed like he was playing the <a href=\"https://www.engadget.com/2017/11/04/ea-introduces-ufc-3/\"><em>UFC 3 </em>video game</a>. Yup, he streamed the mixed martial arts pay-per-view from Little Caesar's Arena in Detroit across his various social channels. AJ Lester went so far as to green-screen himself into the lower right corner of the broadcast, wear a headset and hold a (powered off) DualShock 4 for the <a href=\"https://www.engadget.com/2017/11/10/ufc-performance-center-injury-sports-science/\">entire fight</a>. You can hear him twiddling the analog sticks and watch him play to the camera in the now-viral clip below.</p>\n\t\t\t\t\t\t\t<blockquote align=\"center\" class=\"twitter-tweet\" data-lang=\"en\">\n<p dir=\"ltr\" lang=\"en\">How has he pretended to play a ufc fight on stream to avoid getting copyrighted LOOOOOOOOOOOOOOOOOOOOOOOOL <a href=\"https://t.co/I4ykqwqrTA\">pic.twitter.com/I4ykqwqrTA</a></p>\n&mdash; Aaron (@TheRealSMA) <a href=\"https://twitter.com/TheRealSMA/status/937399039447961601?ref_src=twsrc%5Etfw\">December 3, 2017</a></blockquote>\n\n\n<p><a href=\"http://www.eurogamer.net/articles/2017-12-04-streamer-goes-viral-after-broadcasting-ufc-pay-per-view-while-pretending-to-play-ufc-video-game\">According to <em>Eurogamer</em></a>, Lester hasn't had anyone come knocking just yet, and to help protect himself, he deleted the archived stream. Maybe the proper authorities don't work weekends? Either that, or this isn't the type of thing that's going to get anyone in trouble. The <a href=\"https://www.engadget.com/2017/12/02/ea-s-ufc-3-beta-is-live-this-weekend-on-xbox-one-and-ps4/\"><em>UFC 3</em> open beta</a> runs through 2:59am Eastern, Tuesday and is available on PlayStation 4 and Xbox One.</p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Via: </strong><a target=\"_blank\" href=\"http://www.eurogamer.net/articles/2017-12-04-streamer-goes-viral-after-broadcasting-ufc-pay-per-view-while-pretending-to-play-ufc-video-game\">Eurogamer</a><!--//--></p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Source: </strong><a target=\"_blank\" href=\"https://twitter.com/TheRealSMA/status/937399039447961601?\">TheRealSMA (Twitter)</a><!--//--></p>\n\t\t\t\t\t\t\t"
                        },
                        "category": [
                            "ajlester",
                            "broadcasting",
                            "dmca",
                            "entertainment",
                            "gaming",
                            "internet",
                            "streaming",
                            "twitch",
                            "ufc",
                            "ufc218",
                            "ufc3"
                        ],
                        "creator": {
                            "__prefix": "dc",
                            "__cdata": "Timothy J. Seppala"
                        },
                        "pubDate": "Mon, 04 Dec 2017 11:11:00 -0500",
                        "identifier": {
                            "__prefix": "dc",
                            "__text": "21|23296267"
                        }
                    },
                    {
                        "title": {
                            "__cdata": "Apple to start paying Ireland the billions it owes in back taxes"
                        },
                        "link": "https://www.engadget.com/2017/12/04/apple-paying-ireland-billions-back-taxes/",
                        "guid": {
                            "_isPermaLink": "false",
                            "__text": "https://www.engadget.com/2017/12/04/apple-paying-ireland-billions-back-taxes/"
                        },
                        "comments": "https://www.engadget.com/2017/12/04/apple-paying-ireland-billions-back-taxes/#comments",
                        "description": {
                            "__cdata": "\n\t\t\t\t\t\t<img src=\"https://s.aolcdn.com/hss/storage/midas/4d27ea380af107355c5d70065d42572d/205917979/apple-ed.jpg\" /><p>Last year, the European Commission ruled that Apple's sweetheart tax deal with Ireland was illegal and that the company owed around <a href=\"https://www.engadget.com/2016/08/30/eu-commission-apple-tax-ireland/\">$14.5 billion in back taxes</a>. But Ireland was rather slow to start collecting that cash, which led the Commission to <a href=\"https://www.engadget.com/2017/10/04/europe-takes-ireland-to-court-over-apples-14-5-billion-tax-bil/\">refer the Irish government</a> to the European Court of Justice in October due to Ireland's non-compliance with the 2016 ruling. However, the <a href=\"https://www.wsj.com/articles/apple-agrees-deal-with-ireland-over-15-billion-unpaid-tax-issue-1512392552\"><em>Wall Street Journal</em></a> reports today that the country will finally start collecting those billions of dollars owed by Apple and it may start doing so early next year.</p>\n\t\t\t\t\t\t\t<p>Both Apple and Ireland have fought back against the ruling -- <a href=\"https://www.engadget.com/2016/12/19/ireland-eu-apple-tax-bill-appeal/\">Ireland has said</a> that the European Union overstepped its authority and got some of the country's laws wrong while <a href=\"https://www.engadget.com/2016/12/18/apple-formally-challenges-eu-tax-request/\">Apple has maintained</a> that the amount it's being told to repay was miscalculated. Both are continuing to appeal the decision and the money will sit in an escrow fund while they do so. Ireland has said that negotiating the terms of that fund is what has held up its collection of the money but the European Commission said that the action it has taken against Ireland for failing to follow the 2016 ruling will proceed until the money is collected in full.</p>\n\n<p>Paschal Donohoe, Ireland's finance minister, said that Apple is expected to begin funneling money into the fund during the first quarter of 2018. We may get more information on the fund, its management and the timing of Apple's payments during the company's next earnings report in January.</p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Via: </strong><a target=\"_blank\" href=\"https://www.wsj.com/articles/apple-agrees-deal-with-ireland-over-15-billion-unpaid-tax-issue-1512392552\">Wall Street Journal</a><!--//--></p>\n\t\t\t\t\t\t\t\t\t\t"
                        },
                        "category": [
                            "apple",
                            "business",
                            "escrow",
                            "europeancommission",
                            "europeancourtofjustice",
                            "europeanunion",
                            "gear",
                            "ireland",
                            "taxes"
                        ],
                        "creator": {
                            "__prefix": "dc",
                            "__cdata": "Mallory Locklear"
                        },
                        "pubDate": "Mon, 04 Dec 2017 10:50:00 -0500",
                        "identifier": {
                            "__prefix": "dc",
                            "__text": "21|23296355"
                        }
                    },
                    {
                        "title": {
                            "__cdata": "The best Xbox One games"
                        },
                        "link": "https://www.engadget.com/2017/12/04/the-best-xbox-one-games/",
                        "guid": {
                            "_isPermaLink": "false",
                            "__text": "https://www.engadget.com/2017/12/04/the-best-xbox-one-games/"
                        },
                        "comments": "https://www.engadget.com/2017/12/04/the-best-xbox-one-games/#comments",
                        "description": {
                            "__cdata": "\n\t\t\t\t\t\t<img src=\"https://s.aolcdn.com/hss/storage/midas/ca7eaeb058f370ca49bb9a84ff73f077/205917690/xbox-ed.jpg\" /><p>The <a href=\"https://www.engadget.com/2016/04/22/xbox-one-review-engadget/\">Xbox One</a> has come a long way since its 2013 debut. Microsoft has fixed the console's hardware flaws with the <a href=\"https://www.engadget.com/2016/08/02/xbox-one-s-review/\">One S</a> and the recently released <a href=\"https://www.engadget.com/2017/11/03/xbox-one-x-review/\">One X</a>, but now it has a different problem to address: a dearth of compelling games you can't play anywhere else. In recent years the company has relied on timed third-party exclusives that <a href=\"https://www.engadget.com/2015/07/23/rise-of-the-tomb-raider-hits-ps4-pc-in-2016/\">eventually make their way</a> to other platforms or are also available on Windows and Steam.</p>\n\n<p>It <a href=\"https://www.engadget.com/2017/10/02/nioh-complete-edition-pc-port/\">happens on PS4</a> as well, but the difference there is that Sony has a <a href=\"https://www.engadget.com/2017/11/23/best-playstation-4-games/\">wide assortment of games</a> that you can <em>only</em> play on its console. That's not to say there aren't exclusive games worth playing on Xbox; it's just that they're buried among annual <em>Forza</em> racing games, middling <a href=\"https://www.engadget.com/2015/10/29/halo-5-guardians-combat-devolved/\">modern <em>Halo</em> releases</a> and the testosterone-fueled <a href=\"https://www.engadget.com/2016/10/25/gears-of-war-4-microsoft-research-triton/\"><em>Gears of War</em></a> franchise. Microsoft has promised to break out of that predictable release cadence, though, so the future could be brighter than you may have come to expect. As it stands, these are the best Xbox One games you can play right now.</p>\n\t\t\t\t\t\t\t<h3>AAA vs indie darlings</h3>\n\n<p>Microsoft proved that indie games could have an audience on consoles when it published <em>Braid</em> on the Xbox 360 in 2008. Fast forward nine years and the landscape has changed quite a bit. The Nintendo Switch is increasingly becoming the <a href=\"https://www.engadget.com/2017/11/16/best-nintendo-switch-games/\">home for niche titles</a>, and PlayStation's selection of blockbuster tentpole games grows with every trade-show Sony attends. Faced with this harsh reality, <a href=\"https://www.engadget.com/2017/11/07/microsoft-will-create-game-studios-to-ensure-more-xbox-one-exclu/\">last month</a> Xbox chief Phil Spencer admitted that his team hadn't invested as much in original games as it should. Next year, he's going on a shopping spree for games and development studios to make up for the ones he cancelled and closed.</p>\n\n<p>The company trotted out Capybara Games' <em>Below</em> in the lead-up to the Xbox One's launch in 2013, and since then it's been <a href=\"https://www.engadget.com/2016/08/19/beautiful-indie-game-below-is-delayed-once-again/\">delayed indefinitely</a>. For a while it <a href=\"https://www.engadget.com/2016/10/11/cuphead-delayed-mid2017/\">looked like <em>Cuphead</em></a> would face a similar fate, but as luck would have it Studio MDHR released the game this September after repeated delays. Fullbright's <em>Tacoma</em> had similar misfortunes on the road to release. <em>Limbo</em> studio Playdead's haunting sophomore effort <em>Inside</em> was only exclusive to the Xbox One for a few short months before appearing on PC and PS4. Same goes for <em>Oxenfree</em>.</p>\n\n<p>But that's not necessarily a bad thing. After all, a good game is a good game regardless of where you play it. Case in point: <em>Minecraft</em>, once the biggest indie game in the world, which is now owned by Microsoft. The thing is, until Microsoft proves it can dream beyond <em>Forza</em>, <em>Gears of War</em> and <em>Halo</em> though, indies are your best bet for interesting, inventive ideas on Xbox.</p>\n\n<div class=\"pt-25 pb-25\">\n<hr width=\"100%\" /></div>\n\n<div class=\"half-width left pr-10\"><b>Minecraft</b>\n\n<p><img src=\"https://s.aolcdn.com/hss/storage/midas/d1e5695a3675b8f3bd97cc66b16a5db0/205913253/minecraft.jpg\" /></p>\n\n<p>Since purchasing this blocky freeform building game in 2015, Microsoft has drastically upgraded its graphics (while retaining the game's lo-fi charm). Just as important, it unified nearly every version of <em>Minecraft</em>, allowing people on disparate platforms to play together. Surprisingly, the game didn't lose its soul in the process. It's as charming and soothing as ever, while fancy visual tricks like HDR and detailed textures add some overdue modern flair next year. Combine those additions with <em>Minecraft</em>'s addictive building and survival gameplay and you've got a game that just about anyone can appreciate.</p>\n</div>\n\n<div class=\"half-width right pl-10\"><b>Inside</b>\n\n<p><img src=\"https://s.aolcdn.com/hss/storage/midas/1ecd15491da2c3057a9062e092773de4/205913250/inside.JPG\" /></p>\n\n<p>Playdead's follow-up to its macabre debut <em>Limbo</em> shows what's possible when a studio keeps it simple. <em>Inside</em> is a haunting physics-based platformer that follows a young boy on his journey to, uh, get inside a research facility. As compelling as the game mechanics are, your primary motive for pressing onward should be the game's narrative. While \"dystopian sci-fi\" starring an adolescent might bring <em>The Hunger Games</em> to mind, <em>Inside</em>'s story is more <em>Black Mirror</em> than YA fiction. By the time the credits roll, the frustration of repeatedly getting caught by sentry guards should be a distant memory thanks to one of the craziest endings in modern video games.</p>\n</div>\n\n<div class=\"pt-25 pb-25\">\n<hr width=\"100%\" /></div>\n\n<div class=\"half-width left pr-10\"><b>Overwatch</b>\n\n<p><img data-credit=\"Various\" data-mep=\"2266254\" src=\"https://s.aolcdn.com/hss/storage/midas/6d13062383f15899d04909a177bfb282/205913610/overwatch.jpeg\" alt=\"\" /></p>\n\n<p>If you're playing Blizzard's hero shooter on a console, Xbox One is where you should do it. Playing with friends online feels effortless thanks to the way Xbox Live is baked into every facet of the console. Adjusting the balance between party chat and game audio, adding new friends to your group and even broadcasting your session via Mixer is dead simple on Microsoft's hardware. The game was first released last year, and is showing no signs of getting stale. The team at Blizzard keeps the game humming by regularly releasing new heroes, maps and modes, for free. Just remember, if you're going to play, there are never enough people moving the payload toward the objective. Your team will be grateful for the help.</p>\n</div>\n\n<div class=\"half-width right pl-10\"><b>Cuphead</b>\n\n<p><img src=\"https://s.aolcdn.com/hss/storage/midas/323215587fd2d71eb26b1b80c8565fca/205913247/cuphead-screenshot-devil.jpg\" /></p>\n\n<p>After a series of delays, <em>Cuphead</em> finally came out this year. And it was worth the wait. Studio MDHR's challenging side-scrolling shooter calls to mind classics like <em>Contra</em> and <em>Ghouls 'n Ghosts</em>, with retro visuals to match. Rather than pixel art reminiscent of the 8- or 16-bit era, though, <em>Cuphead</em> features hand-drawn animation inspired by cartoons from the 1930s. Keep in mind, this game isn't for the faint of heart. You'll simultaneously have to memorize level layouts, enemy placements and bosses' attack patterns if you want to succeed. That's in addition to reminding yourself to duck incoming fire and not get distracted by the inventive audio and visual design. If that seems like a tall order, it is. But you like a challenge, right?</p>\n</div>\n\n<div class=\"pt-25 pb-25\">\n<hr width=\"100%\" /></div>\n\n<div class=\"half-width left pr-10\"><b>Halo: The Master Chief Collection</b>\n\n<p><img src=\"https://s.aolcdn.com/hss/storage/midas/8c11c8ee117b5ae1246f3e88c952c6dd/205913246/h4campaigndawnfirstperson03.jpg\" /></p>\n\n<p><em>Halo</em> has been Microsoft's golden goose since the original Xbox launched in 2001. But recent years haven't been kind to the series, which has seen some duds. That's why we recommend <em>The Master Chief Collection</em> (<em>MCC</em>), a greatest-hits collection of the franchise's best releases, with upgraded visuals and unique twists like playlists that group campaign missions by theme. Want to play the opening level from each game in succession? Have at it. Same goes if you'd rather play nothing but vehicle-based missions. Additionally, <em>MCC</em> takes every map from the franchise's multiplayer suite online. The player population has since migrated to <em>Halo 5: Guardians</em>, but if you want to recreate the experience of playing capture the flag on Blood Gulch at your first Xbox LAN party, this is your best bet.</p>\n</div>\n\n<div class=\"half-width right pl-10\"><b>Forza Horizon 3</b>\n\n<p><img src=\"https://s.aolcdn.com/hss/storage/midas/4b9c523473821e6f9b0cfca48f3f428e/205913248/forza.JPG\" /></p>\n\n<p><em>Forza Horizon 3</em> isn't just Microsoft's most recent off-road racer -- it's the best entry in the <em>Horizon</em> series to date. And you don't have to be a gearhead to have a blast tooling around the game's Australian outback. Unlike the mainline <em>Forza Motorsport</em> series, the emphasis here is on pedal-to-the-metal, arcade-inspired fun that emphasizes silliness over simulation. But that doesn't mean <em>Horizon</em> isn't serious about accuracy; physics still matter, and you can of course upgrade and tune your favorite ride to your heart's content. Need a break from the Outback's beaches and jungles? The \"Blizzard Mountain\" expansion takes the terror out of driving in inclement weather, while the Hot Wheels add-on puts the toy cars and their trademark orange loop-de-loop tracks on your flatscreen.</p>\n</div>\n\n<div class=\"pt-25 pb-25\">\n<hr width=\"100%\" /></div>\n\n<div class=\"half-width left pr-10\"><b>Alan Wake</b>\n\n<p><img data-credit=\"Various\" data-mep=\"2266255\" src=\"https://o.aolcdn.com/images/dims?crop=640%2C360%2C0%2C0&amp;quality=85&amp;format=jpg&amp;resize=1600%2C900&amp;image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2F23631c2839503a3dd292485c0bf772ea%2F205913604%2Falanwake.jpg&amp;client=a1acac3e1b3290917d92&amp;signature=819bb350f1db960e8f6ad10b6ac1ec6f5340b52d\" alt=\"\" /></p>\n\n<p>The Xbox One's biggest advantage over the competition is that you can play a huge swath of the Xbox 360's catalog on it. That includes <em>Alan Wake</em>, Remedy Games first Microsoft exclusive from 2009. If the recent <em>Twin Peaks</em> revival made you want to take a trip to the Pacific Northwest, consider this game about a horror novelist whose creations are coming back to haunt him. The sleepy town of Bright Falls is being corrupted by a dark presence that possesses people and inanimate objects, even turning wheelbarrows into fearsome enemies. You'll need more than just firearms to defeat them, too; flashlights and flares are as important as firearms, as you need to \"burn\" the darkness out of an enemy before destroying it for good.</p>\n</div>\n\n<div class=\"half-width right pl-10\"><b>Ori and the Blind Forest</b>\n\n<p><img src=\"https://s.aolcdn.com/hss/storage/midas/5be68ca398a479f516afc4b4a9967a42/205913254/oriscreenforlornd-jpg1.jpg\" /></p>\n\n<p><em>Ori and the Blind Forest</em> will break your heart in the first 10 minutes. After that, things get easier -- at least from an emotional standpoint. <em>Ori</em> follows the tradition of <em>Super Metroid</em> and <em>Castlevania: Symphony of the Night</em> by giving you a huge interconnected 2D world to explore, where secrets are hidden in plain sight and familiar areas give way to new ones as you unlock abilities for Ori. Gorgeous art and music complement the onscreen exploration; if you're a fan of physics-based platforming &agrave; la <em>Super Meat Boy</em>, you're in for a treat. Now's the perfect time to try the game, too: Microsoft said earlier this year that it's ordered a sequel. But with no release date in sight, you should have plenty of time to explore the game's mysteries.</p>\n</div>\n\n<div class=\"pt-25 pb-25\">\n<hr width=\"100%\" /></div>\n\n<div class=\"half-width left pr-10\"><b>Killer Instinct</b>\n\n<p><img src=\"https://s.aolcdn.com/hss/storage/midas/2b2c02b4cfd116cf4132d5ec6c8152f5/205913251/killer-instinct-xbox-one-1600-ed.jpg\" /></p>\n\n<p>When Microsoft bought Rare Ware back in 2002, a big questions was whether the company would resurrect the long-dormant fighting franchise <em>Killer Instinct</em>. Eleven years later, a series reboot served as an anchor for the Xbox One's meager launch lineup. In the four years since, development passed to <em>Divekick</em> studio Iron Galaxy. Meanwhile, Microsoft added <em>Gears of War</em> bad-guy General Raam, <em>Halo</em>'s third-fiddle protagonist The Arbiter, and Rash from Rare's <em>Battletoads</em> series to the roster. The game now boasts almost 30 fighters, and its combo-based pugilism feels as satisfying as it did on the Super NES and N64. A quasi-story mode helps keep things interesting for solo players, and you can even fight against AI based off the fighting style of people on your friends list. Think of it like <em>Forza</em>'s Drivatar system, but for punching faces.</p>\n</div>\n\n<div class=\"half-width right pl-10\"><b>Quantum Break</b>\n\n<p><img src=\"https://s.aolcdn.com/hss/storage/midas/49143d0daaafbb39b788a00e83e2ebaf/205913255/quantum-break-time-stop-ed.jpg\" /></p>\n\n<p><em>Quantum Break</em> is at once a cinema-inspired third-person shooter and an interactive TV drama where the choices you make in the game affect how each episode of the show plays out. Manipulating time goes beyond the game's narrative and impacts how you solve puzzles and take out enemies. The game's surreal vistas are particularly impressive. Thanks to a glitch in the space-time continuum, time itself is breaking down. Which means every now and again, everything in a given scene will go into still-frame, be it a shipping barge crashing into a suspension bridge, or a helicopter attacking an office building. It makes for some incredibly cool platforming sequences. And if you've just picked up an Xbox One X, Remedy went back over the game to make those \"stutters\" (and everything else) perform better on Microsoft's new hardware.</p>\n</div>\n\n<div class=\"pt-25 pb-25\">\n<hr width=\"100%\" /></div>\n\n<div class=\"half-width left pr-10\"><b>Fru</b>\n\n<p><img src=\"https://s.aolcdn.com/hss/storage/midas/4fd6c405104d0950acd1136aebaa1d6b/205913249/fruscreen.jpg\" /></p>\n\n<p><em>Fru</em> wasn't just one of the last Kinect games -- it was one of the best reasons to own Microsoft's do-all sensor. Rather than use the camera array and microphones for clunky motion and voice controls, <em>Fru</em> takes advantage of a different aspect of the hardware: its ability to read silhouettes in real time. The game is a platformer at its core, and to cross certain gaps or find hidden treasures, you'll need to position yourself in a way that allows the fox-masked protagonist to run along or inside your silhouette. You can make the game even more challenging by trying to play solo as you contort your body in front of the sensor. Or, you can tap your yoga-master best friend to hold their Vriksasana pose while you make your way ever rightward along the game's 2D plane.</p>\n</div>\n\t\t\t\t\t\t\t\t\t\t\t\t\t"
                        },
                        "category": [
                            "343industries",
                            "alanwake",
                            "av",
                            "blizzard",
                            "cuphead",
                            "forza",
                            "forzahorizon3",
                            "fru",
                            "gaming",
                            "halo",
                            "halothemastercheifcollection",
                            "inside",
                            "irongalaxy",
                            "killerinstinct",
                            "microsoft",
                            "minecraft",
                            "mojang",
                            "oriandtheblindforest",
                            "overwatch",
                            "playdead",
                            "playgroundgames",
                            "quantumbreak",
                            "remedyentertainment",
                            "studiomdhr",
                            "xbox",
                            "xbox360",
                            "xboxone"
                        ],
                        "creator": {
                            "__prefix": "dc",
                            "__cdata": "Timothy J. Seppala"
                        },
                        "pubDate": "Mon, 04 Dec 2017 10:30:00 -0500",
                        "identifier": {
                            "__prefix": "dc",
                            "__text": "21|23294481"
                        }
                    },
                    {
                        "title": {
                            "__cdata": "Android will flag snooping apps that don’t warn users"
                        },
                        "link": "https://www.engadget.com/2017/12/04/google-android-privacy-apps-warnings/",
                        "guid": {
                            "_isPermaLink": "false",
                            "__text": "https://www.engadget.com/2017/12/04/google-android-privacy-apps-warnings/"
                        },
                        "comments": "https://www.engadget.com/2017/12/04/google-android-privacy-apps-warnings/#comments",
                        "description": {
                            "__cdata": "\n\t\t\t\t\t\t<img src=\"https://s.aolcdn.com/hss/storage/midas/c93b943f44025da65534b52f82a7d39d/205917398/google-play-blocking-bad-apps-2017-12-04-01.jpg\" /><p>Google, a company that known to keep <a href=\"https://www.engadget.com/2017/11/21/android-has-collected-location-despire-sharing-settings/\">uncomfortably close tabs</a> on users, is taking new measures to ensure that other Android apps don't do the same without proper warning. The company's Safe Browsing team has <a href=\"https://security.googleblog.com/2017/12/additional-protections-by-safe-browsing.html\">unveiled</a> stricter enforcement of its \"unwanted software policy,\" warning users off apps that collect your personal data without consent. Google's search engine will even scare users away from websites that offer up apps violating its policies.</p>\n\t\t\t\t\t\t\t<p>Google will flag bad apps with warnings on Play via <a href=\"https://www.engadget.com/2017/07/19/google-play-protect-rollout/\">Google Play Protect</a>, or with the dreaded red boxes that discourage Search users from proceeding to bad sites. To avoid ending up on its naughty list, apps that use personal user data like your phone number, email or location data \"will be required to prompt users and to provide their own privacy policy in the app,\" Google says. You must also provide consent each time an app transmits personal info \"unrelated to the functionality of the app.\"</p>\n\n<p>The search giant is cracking down hard on privacy issues, having recently banned apps that display ads in your <a href=\"https://www.engadget.com/2017/12/01/google-play-bans-apps-with-lockscreen-ads/\">lock screen</a>, for instance. That's a noble effort, but Google itself has been conspicuous lately for violating user trust. It was found to have been <a href=\"https://www.engadget.com/2017/11/21/android-has-collected-location-despire-sharing-settings/\">tracking</a> users' cellphone tower positions and relaying the data back to its servers, ostensibly to improve messaging speed.</p>\n\n<p>It has since halted the practice, but this was happening regardless of whether you had opted in, even if you switched off your cellular service. Since it was neither informing users nor respecting their intentions, Google itself would have been in violation of its new privacy policy.</p>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<p><strong>Source: </strong><a target=\"_blank\" href=\"https://security.googleblog.com/2017/12/additional-protections-by-safe-browsing.html\">Google Security Blog</a><!--//--></p>\n\t\t\t\t\t\t\t"
                        },
                        "category": [
                            "app",
                            "apps",
                            "gear",
                            "google",
                            "googleplay",
                            "internet",
                            "privacy",
                            "safebrowsing",
                            "search",
                            "security",
                            "userinfo"
                        ],
                        "creator": {
                            "__prefix": "dc",
                            "__cdata": "Steve Dent"
                        },
                        "pubDate": "Mon, 04 Dec 2017 10:01:00 -0500",
                        "identifier": {
                            "__prefix": "dc",
                            "__text": "21|23296179"
                        }
                    },
                    {
                        "title": {
                            "__cdata": "Chrome for Android will soon get HDR video support"
                        },
                        "link": "https://www.engadget.com/2017/12/04/chrome-android-hdr-video-update/",
                        "guid": {
                            "_isPermaLink": "false",
                            "__text": "https://www.engadget.com/2017/12/04/chrome-android-hdr-video-update/"
                        },
                        "comments": "https://www.engadget.com/2017/12/04/chrome-android-hdr-video-update/#comments",
                        "description": {
                            "__cdata": "\n\t\t\t\t\t\t<img src=\"https://s.aolcdn.com/hss/storage/midas/253d8a54b6b137eccaa22380630e04b1/205916790/mg-2158-1.jpg\" /><p>Google Chrome for Android is about to join <a href=\"https://www.engadget.com/2017/09/08/youtube-hdr-pixel-galaxy-s8-lg-v30/\">YouTube</a> and <a href=\"https://www.engadget.com/2017/05/10/netflix-hdr-arrives-on-android-but-only-for-the-lg-g6/\">Netflix</a> among the apps that can support HDR playback. Two <a href=\"https://chromium-review.googlesource.com/c/chromium/src/+/755952\">recent</a> <a href=\"https://chromium-review.googlesource.com/c/chromium/src/+/756443\">updates</a> on the Chromium Gerrit (spotted by <a href=\"https://www.xda-developers.com/hdr-video-playback-support-chrome-for-android/\"><i>XDA</i></a>) gave the game away. For those in the dark, <a href=\"https://www.engadget.com/2017/08/28/samsung-hdr-10-plus-licensing/\">HDR</a> (high dynamic range) amps up the color and contrast significantly compared to standard dynamic range to offer a more realistic picture.</p>\n\t\t\t\t\t\t\t<p>Of course, you'll require a HDR-capable smartphone to try it out (see <a href=\"https://www.engadget.com/2017/04/13/lg-g6-review/\">LG</a> <a href=\"https://www.engadget.com/2017/10/28/lg-v30-review/\">and</a> <a href=\"https://www.engadget.com/2017/09/05/samsung-galaxy-note-8-review/\">Samsung's</a> <a href=\"https://www.engadget.com/2017/04/18/samsung-galaxy-s8-and-s8-plus-review/\">flagships</a>, <a href=\"https://www.engadget.com/2017/09/04/samsung-galaxy-note-8-sony-xperia-xz1-netflix-hdr/\">Sony Xperia XZ1</a>, and <a href=\"https://www.engadget.com/2017/08/04/sony-xperia-xz-premium-netflix-hdr/\">Sony Xperia XZ Premium</a>). Oh, and you'll also need to be on the lookout for video carrying the HDR tag, which is sorely lacking at present. At least with the future Chrome update, you'll be able to scour the entirety of the web for content.</p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Via: </strong><a target=\"_blank\" href=\"https://www.xda-developers.com/hdr-video-playback-support-chrome-for-android/\">XDA</a><!--//--></p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Source: </strong><a target=\"_blank\" href=\"https://chromium-review.googlesource.com/c/chromium/src/+/756443\">Chromium Gerrit (1)</a><!--//-->, <a target=\"_blank\" href=\"https://chromium-review.googlesource.com/c/chromium/src/+/756443\">(2)</a><!--//--></p>\n\t\t\t\t\t\t\t"
                        },
                        "category": [
                            "android",
                            "av",
                            "chrome",
                            "gear",
                            "google",
                            "hdr",
                            "mobile"
                        ],
                        "creator": {
                            "__prefix": "dc",
                            "__cdata": "Saqib Shah"
                        },
                        "pubDate": "Mon, 04 Dec 2017 09:44:00 -0500",
                        "identifier": {
                            "__prefix": "dc",
                            "__text": "21|23295961"
                        }
                    },
                    {
                        "title": {
                            "__cdata": "UK watchdog has to remind MPs not to share their passwords"
                        },
                        "link": "https://www.engadget.com/2017/12/04/uk-ico-mp-shared-password/",
                        "guid": {
                            "_isPermaLink": "false",
                            "__text": "https://www.engadget.com/2017/12/04/uk-ico-mp-shared-password/"
                        },
                        "comments": "https://www.engadget.com/2017/12/04/uk-ico-mp-shared-password/#comments",
                        "description": {
                            "__cdata": "\n\t\t\t\t\t\t<img src=\"https://o.aolcdn.com/images/dims?crop=1950%2C1214%2C0%2C0&amp;quality=85&amp;format=jpg&amp;resize=1600%2C996&amp;image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2Fbe3b68c47bdcc21dcc0746cb3d02ab7a%2F201563019%2F2.14462723.jpg&amp;client=a1acac3e1b3290917d92&amp;signature=df5745a98e3e7938e4d462bfd1d7eb236ca5f551\" /><p>With the <a href=\"https://www.engadget.com/2017/11/29/uber-uk-hack-numbers-revealed/\">number of computer-based attacks steadily rising in the UK</a>, Britons have been warned over and over again to use strong passwords, to ensure they're not recycled and to never disclose them to a third party. So when three Conservative MPs came out over the weekend admitting to sharing their credentials with interns and other members of staff, it's easy to see why the UK's data watchdog is none too pleased.</p>\n\n<p></p>\n\t\t\t\t\t\t\t<p>In the space of the last few days, Nadine Dorries MP, Nick Boles MP and Crispin Blunt MP claimed that they share passwords with fellow office workers in order to perform tasks on their behalf. <a href=\"https://twitter.com/NadineDorries/status/937019367572803590\">Ms Dorries said</a> that her team logged into her computer using her details \"everyday,\" which was a message that was <a href=\"https://twitter.com/NickBoles/status/937403837433958400\">later echoed by Mr Boles</a> in a tweeted reply. <a href=\"https://www.channel4.com/news/crispin-blunt-on-damian-green-it-would-be-astonishing-if-he-were-lying\">Mr Blunt later explained on <em>Channel 4 News</em></a> that he allowed such actions because \"we don't live a life where we are actually able to spend lots of time sitting in our office browsing our computers.\"</p>\n\n<center>\n<blockquote class=\"twitter-tweet\" data-lang=\"en\">\n<p dir=\"ltr\" lang=\"en\">My staff log onto my computer on my desk with my login everyday. Including interns on exchange programmes. For the officer on <a href=\"https://twitter.com/BBCNews?ref_src=twsrc%5Etfw\">@BBCNews</a> just now to claim that the computer on Greens desk was accessed and therefore it was Green is utterly preposterous !!</p>\n&mdash; Nadine Dorries (@NadineDorries) <a href=\"https://twitter.com/NadineDorries/status/937019367572803590?ref_src=twsrc%5Etfw\">December 2, 2017</a></blockquote>\n</center>\n\n<p>The admissions came after claims that First Secretary of State Damian Green should be held responsible for viewing pornography on his work computer. Mr Green denied doing so and has since been urged to step down.</p>\n\n<p>Today, the Information Commissioner's Office acknowledged that it had been made aware of reports that MPs are happy to pass around their logins and passwords and has begun talking to the \"relevant parliamentary authorities\":</p>\n\n<center>\n<blockquote class=\"twitter-tweet\" data-lang=\"en\">\n<p dir=\"ltr\" lang=\"en\">We're aware of reports that MPs share logins and passwords and are making enquiries of the relevant parliamentary authorities. We would remind MPs and others of their obligations under the Data Protection Act to keep personal data secure. <a href=\"https://t.co/FLPeP8M7c8\">https://t.co/FLPeP8M7c8</a></p>\n&mdash; ICO (@ICOnews) <a href=\"https://twitter.com/ICOnews/status/937654177571983362?ref_src=twsrc%5Etfw\">December 4, 2017</a></blockquote>\n</center>\n\n<p>Sharing logins isn't illegal but <a href=\"http://www.parliament.uk/documents/commons-resources/Staff-handbook/chapter-23-information-security.pdf\">the House of Commons Staff Handbook categorically states</a> that party employees should not share their passwords. However, MPs are allowed to decide for themselves how they interpret such instructions.</p>\n\n<p>The common reason for interns and other staff members to access MP computers appears to be to update calendars and respond to emails on a minister's behalf, tasks that could be handled remotely via a shared inbox or grouped computer policies.</p>\n\n<p>As a nation, politicians aren't expected to be cybersecurity experts, but they are expected to be aware of basic online and computer protections. That's why today's warning, while embarrassing, is totally necessary.</p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Via: </strong><a target=\"_blank\" href=\"http://www.bbc.co.uk/news/technology-42225214\">BBC News</a><!--//--></p>\n\t\t\t\t\t\t\t\t\t\t"
                        },
                        "category": [
                            "gear",
                            "ico",
                            "internet",
                            "login",
                            "mp",
                            "online",
                            "parliament",
                            "passsword",
                            "politics",
                            "safety",
                            "twitter"
                        ],
                        "creator": {
                            "__prefix": "dc",
                            "__cdata": "Matt Brian"
                        },
                        "pubDate": "Mon, 04 Dec 2017 09:39:00 -0500",
                        "identifier": {
                            "__prefix": "dc",
                            "__text": "21|23296273"
                        }
                    },
                    {
                        "title": {
                            "__cdata": "Uber joins public transport group to win back city trust"
                        },
                        "link": "https://www.engadget.com/2017/12/04/uber-joins-public-transport-group-to-win-back-city-trust/",
                        "guid": {
                            "_isPermaLink": "false",
                            "__text": "https://www.engadget.com/2017/12/04/uber-joins-public-transport-group-to-win-back-city-trust/"
                        },
                        "comments": "https://www.engadget.com/2017/12/04/uber-joins-public-transport-group-to-win-back-city-trust/#comments",
                        "description": {
                            "__cdata": "\n\t\t\t\t\t\t<img src=\"https://s.aolcdn.com/hss/storage/midas/e7d5bb115fedfd1ef0e1925a9aed1b98/205917767/uber-ed2.jpg\" /><p>Uber's devil-may-care attitude to, uh, pretty much everything has managed to put noses out of joint all across the globe. Its new CEO, <a href=\"https://www.engadget.com/2017/08/29/uber-ceo-dara-khosrowshahi/\">Dara Khosrowshahi</a>, is hoping to build some bridges that <a href=\"https://www.engadget.com/2017/06/21/uber-ceo-travis-kalanick-resigns/\">Travis Kalanick</a> burned by making peace with local transport providers. Consequently, the outfit is signing up with the International Association of Public Transport, an advocacy industry group for public transport providers. Its membership includes <a href=\"https://www.engadget.com/2017/09/08/taxify-suspends-rides-tfl-london-investigation/\">Transport for London</a>, which <a href=\"https://www.engadget.com/2017/09/22/uber-london-operating-licence-denied/\">recently revoked Uber's</a> <a href=\"https://www.engadget.com/2017/10/13/uber-appeals-london-licence-loss/\">license to operate in the city</a>.</p>\n\t\t\t\t\t\t\t<p>This new-found spirit of co-operation will be fostered with a series of training sessions designed around fixing the beginning and end of people's daily journeys. In addition, Uber's Andrew Salzberg told <a href=\"https://www.reuters.com/article/uk-uber-transport/uber-joins-forces-with-global-public-transport-association-idUSKBN1DY0DS\"><em>Reuters</em></a> that the company is pledging to both work better with infrastructure providers and reduce congestion. Specifically, that Uber will somehow encourage people to adopt shared modes of transport rather than going it alone. That sort of talk is, unfortunately, pretty cheap, but given how low Uber's reputation is right now, cities and regulators are hoping the company will now begin to turn over a new leaf.</p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Via: </strong><a target=\"_blank\" href=\"https://www.reuters.com/article/uk-uber-transport/uber-joins-forces-with-global-public-transport-association-idUSKBN1DY0DS\">Reuters</a><!--//--></p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Source: </strong><a target=\"_blank\" href=\"http://www.uitp.org/sites/default/files/NewPlatform_pressreleasefinal.pdf\">UITP</a><!--//--></p>\n\t\t\t\t\t\t\t"
                        },
                        "category": [
                            "andrewsalzberg",
                            "darakhosrowshahi",
                            "gear",
                            "internationalassociationofpublictransport",
                            "tfl",
                            "transportation",
                            "uber"
                        ],
                        "creator": {
                            "__prefix": "dc",
                            "__cdata": "Daniel Cooper"
                        },
                        "pubDate": "Mon, 04 Dec 2017 09:22:00 -0500",
                        "identifier": {
                            "__prefix": "dc",
                            "__text": "21|23296077"
                        }
                    },
                    {
                        "title": {
                            "__cdata": "HP’s wearable VR system is an unnecessary luxury"
                        },
                        "link": "https://www.engadget.com/2017/12/04/hp-omen-x-vr-backpack-review/",
                        "guid": {
                            "_isPermaLink": "false",
                            "__text": "https://www.engadget.com/2017/12/04/hp-omen-x-vr-backpack-review/"
                        },
                        "comments": "https://www.engadget.com/2017/12/04/hp-omen-x-vr-backpack-review/#comments",
                        "description": {
                            "__cdata": "\n\t\t\t\t\t\t<img src=\"https://o.aolcdn.com/images/dims?crop=1600%2C997%2C0%2C0&amp;quality=85&amp;format=jpg&amp;resize=1600%2C997&amp;image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2F591ba655ffe4437855544c0db9e60a7c%2F205912893%2FHP%2BVR%2Bgallery%2B27.jpg&amp;client=a1acac3e1b3290917d92&amp;signature=4f5322a30b29559ee69a832357e1ec56fb70b126\" /><p>You've never seen a PC like <a href=\"https://www.engadget.com/2017/06/06/hp-vr-backpack-dock-desktop/\">HP's Omen X Compact Desktop</a>. It's a powerful gaming rig, and it's small enough to do double duty as a pseudo console under your TV. But it also has a built-in battery. And when attached to HP's revamped VR backpack accessory and its <a href=\"https://www.engadget.com/2017/05/11/microsoft-acer-hp-windows-mixed-reality-headsets-pre-order/\">mixed reality headset</a>, you can experience high-end virtual reality in a completely new and freeing way. The only problem? The entire package will cost you close to $3,500. That puts it far out of reach for the vast majority of gamers, even those who don't mind shelling out for the latest hardware. </p>\n\t\t\t\t\t\t\t<h3>Hardware</h3>\n\n<p>True to its name, the Omen X Compact Desktop is sleek and relatively portable. On its own, it weighs 5.5lbs -- lighter than popular gaming notebooks from Alienware and Razer. The desktop also shares the same aesthetic as <a href=\"https://www.engadget.com/2017/09/07/hp-omen-15-review/\">HP's Omen 15</a> and <a href=\"https://www.engadget.com/2017/08/23/hp-omen-x-laptop-hands-on/\">Omen X</a> laptops. It's got a sharp, angular design with a stylish black case and red accents. It makes a statement on its own, but it really shines when you place it in the bundled dock, which turns it into a monument to PC gaming.</p>\n\n<p>The dock gives you an easy way to connect the desktop to your workspace, and quickly remove it, without having to deal with plugging in cables. The front features two USB 3.0 ports, as well as USB-C. While the back has another 3 USB connections, an ethernet jack, as well as DisplayPort and HDMI. There's also a power connection for recharging the batteries from HP's VR backpack.</p>\n\n<p><img data-credit=\"Devindra Hardawar/AOL\" data-mep=\"2266229\" src=\"https://o.aolcdn.com/images/dims?crop=1600%2C962%2C0%2C0&amp;quality=85&amp;format=jpg&amp;resize=1600%2C962&amp;image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2Fa0b20a33f36a58fc97d345752192461f%2F205912881%2FHP%2BVR%2Bgallery%2B12.jpg&amp;client=a1acac3e1b3290917d92&amp;signature=7431d96839cfd3ff295a6ee7059ee5644fbd1ee5\" alt=\"\" /></p>\n\n<p>The compact desktop, meanwhile, has two USB ports up top, USB-C, HDMI, MiniDisplayPort, a headphone jack, and, conveniently enough, a power connection for the HTC Vive headset. On top of that, there are also two USB ports along the lower side of the desktop. There's no SD card slot on the the computer or its dock, which seems like a surprising omission for such a fully featured setup.</p>\n\n<p>As for HP's Mixed Reality headset, it's in line with what we've seen from <a href=\"https://www.engadget.com/2017/05/11/microsoft-acer-hp-windows-mixed-reality-headsets-pre-order/\">other Windows VR gear</a>. It's relatively light and, most importantly, easy to put on and take off. There's a liberal amount of padding around the face area and head band, which insures a comfortable fit. There also aren't too many straps to mess with: you just loosen the headband with a rear dial, and tighten it once you've put it on. Thankfully, you can flip the visor portion up, allowing you to see the real world without removing the entire devices. That's one of the more useful features we've seen from Mixed Reality devices.</p>\n\n<p><img data-credit=\"Devindra Hardawar/AOL\" data-mep=\"2266230\" src=\"https://o.aolcdn.com/images/dims?crop=1600%2C900%2C0%2C0&amp;quality=85&amp;format=jpg&amp;resize=1600%2C900&amp;image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2Ff59ecc2c85684d5b9bfae0c4bb45202f%2F205912915%2FHP%2BVR%2Bgallery%2B1.jpg&amp;client=a1acac3e1b3290917d92&amp;signature=42219a8541c3ec19031daceedb7eb3461daecd5d\" alt=\"\" /></p>\n\n<p>Each of the headset's lenses features a 1,440 by 1,440 resolution at 90Hz -- the same specs we've seen on most Windows Mixed Reality headsets. There's a headphone jack along the bottom (you'll have to supply your own), as well as a short built-in cable. The latter is particularly helpful, since it lets you use a long cable to connect to the dektop normally, but you can also swap it out for a shorter cable to use with the backpack.</p>\n\n<p>As you'd expect, HP also includes two mixed reality controllers. Each features a large sensor ring for spatial tracking, as well as a thumbstick, and a trackpad that can also be recognized as four separate buttons. There are also the usual things we see on every VR controller these days: trigger and grab buttons, along with menu and home options.</p>\n\n<section data-eng-breakout=\"93463\" data-eng-breakout-type=\"gallery\" style=\"outline: 2px dashed red;\"></section>\n\n<p>Tying all of these new gaming devices together is HP's VR backpack, which is meant to let you experience virtual reality without being tied down to a large desktop. It sports padded straps and a plastic panel, which the Compact Desktop slides onto securely. It also has two side holsters for battery packs. HP includes four batteries with the backpack, so you can keep one pair charged while you're using the other.</p>\n\n<h3>Performance and battery life</h3>\n\n<center>\n<table class=\"table table-striped\">\n\t<thead>\n\t\t<tr>\n\t\t\t<th style=\"text-align: left;\"></th>\n\t\t\t<th style=\"text-align: left;\">PCMark 7</th>\n\t\t\t<th style=\"text-align: left;\">PCMark 8 (Creative Accelerated)</th>\n\t\t\t<th style=\"text-align: left;\">3DMark 11</th>\n\t\t\t<th style=\"text-align: left;\">3DMark (Sky Diver)</th>\n\t\t\t<th style=\"text-align: left;\">ATTO (top reads/writes)</th>\n\t\t</tr>\n\t</thead>\n\t<tbody>\n\t\t<tr>\n\t\t\t<td style=\"text-align: left;\">HP Omen Compact Desktop (2.9Ghz-3.9GHz i7-7820HK, NVIDIA GTX 1080 [overclocked])</td>\n\t\t\t<td>7,040</td>\n\t\t\t<td>N/A</td>\n\t\t\t<td>E21,786 / P19,286 / X9,144</td>\n\t\t\t<td>34,094</td>\n\t\t\t<td style=\"text-align: left;\">3.1 GB/s / 1.65 GB/s</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"text-align: left;\">HP Omen 15 (2.8GHz Intel Core i7-7700HQ, NVIDIA GTX 1060)</td>\n\t\t\t<td>6,727</td>\n\t\t\t<td>6,436</td>\n\t\t\t<td>E14,585 / P11,530 / X4,417</td>\n\t\t\t<td>20,659</td>\n\t\t\t<td style=\"text-align: left;\">1.7 GB/s / 704 MB/s</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"text-align: left;\">ASUS ROG Zephyrus (2.8GHz Intel Core i7-7700HQ, NVIDIA GTX 1080)</td>\n\t\t\t<td>6,030</td>\n\t\t\t<td>7,137</td>\n\t\t\t<td>E20,000 / P17,017 / X7,793</td>\n\t\t\t<td>31,624</td>\n\t\t\t<td style=\"text-align: left;\">3.4 GB/s / 1.64 GB/s</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"text-align: left;\"><a href=\"https://www.engadget.com/2017/05/29/alienware-15-review/\">Alienware 15</a> (2.8GHz Intel Core i7-7700HQ, NVIDIA GTX 1070)</td>\n\t\t\t<td>6,847</td>\n\t\t\t<td>7,100</td>\n\t\t\t<td>E17,041 / P16,365</td>\n\t\t\t<td>20,812</td>\n\t\t\t<td style=\"text-align: left;\">2.9 GB/s / 0.9 GB/s</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"text-align: left;\"><a href=\"https://www.engadget.com/2017/02/21/alienware-13-2017-kaby-lake-review/\">Alienware 13</a> (2.8GHz Intel Core i7-7700HQ, NVIDIA GTX 1060)</td>\n\t\t\t<td>4,692</td>\n\t\t\t<td>4,583</td>\n\t\t\t<td>E16,703 / P12,776</td>\n\t\t\t<td>24,460</td>\n\t\t\t<td style=\"text-align: left;\">1.78 GB/s / 1.04 GB/s</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"text-align: left;\"><a href=\"https://www.engadget.com/2016/12/19/razer-blade-pro-review-2016/\">Razer Blade Pro</a> 2016 (2.6GHz Intel Core i7-6700HQ, NVIDIA GTX 1080)</td>\n\t\t\t<td>6,884</td>\n\t\t\t<td>6,995</td>\n\t\t\t<td>E18,231 / P16,346</td>\n\t\t\t<td>27,034</td>\n\t\t\t<td style=\"text-align: left;\">2.75 GB/s / 1.1 GB/s</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"text-align: left;\"><a href=\"https://www.engadget.com/2016/12/15/asus-rog-gl502-strix-review/\">ASUS ROG Strix GL502VS</a> (2.6GHz Intel Core i7-6700HQ , NVIDIA GTX 1070)</td>\n\t\t\t<td>5,132</td>\n\t\t\t<td>6,757</td>\n\t\t\t<td>E15,335 / P13,985</td>\n\t\t\t<td>25,976</td>\n\t\t\t<td style=\"text-align: left;\">2.14 GB/s / 1.2 GB/s</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"text-align: left;\"><a href=\"https://www.engadget.com/2016/12/12/hp-spectre-x360-review-2016/\">HP Spectre x360</a> (2016, 2.7GHz Core i7-7500U, Intel HD 620)</td>\n\t\t\t<td>5,515</td>\n\t\t\t<td>4,354</td>\n\t\t\t<td>E2,656 / P1,720 / X444</td>\n\t\t\t<td style=\"text-align: left;\">3,743</td>\n\t\t\t<td>1.76 GB/s / 579 MB/s</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"text-align: left;\"><a href=\"https://www.engadget.com/2017/01/02/lenovo-yoga-910-review/\">Lenovo Yoga 910</a> (2.7GHz Core i7-7500U, 8GB, Intel HD 620)</td>\n\t\t\t<td>5,822</td>\n\t\t\t<td>4,108</td>\n\t\t\t<td>\n\t\t\t<p style=\"text-align: left;\">E2,927 / P1,651 / X438</p>\n\t\t\t</td>\n\t\t\t<td>3,869</td>\n\t\t\t<td style=\"text-align: left;\">1.59 GB/s / 313 MB/s</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"text-align: left;\"><a href=\"https://www.engadget.com/2016/10/22/razer-blade-stealth-core-review/\">Razer Blade</a> (Fall 2016) (2.7GHz Intel Core-i7-7500U, Intel HD 620)</td>\n\t\t\t<td>5,462</td>\n\t\t\t<td>3,889</td>\n\t\t\t<td>E3,022 / P1,768</td>\n\t\t\t<td>4,008</td>\n\t\t\t<td style=\"text-align: left;\">1.05 GB/s / 281 MB/s</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"text-align: left;\"><a href=\"https://www.engadget.com/2016/10/22/razer-blade-stealth-core-review/\">Razer Blade (Fall 2016) + Razer Core</a> (2.7GHz Intel Core-i7-7500U, NVIDIA GTX 1080)</td>\n\t\t\t<td>5,415</td>\n\t\t\t<td>4,335</td>\n\t\t\t<td>E11,513 / P11,490</td>\n\t\t\t<td>16,763</td>\n\t\t\t<td style=\"text-align: left;\">1.05 GB/s / 281 MB/s</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"text-align: left;\"><a href=\"https://www.engadget.com/2016/10/05/asus-zenbook-3-review/\">ASUS ZenBook 3</a> (2.7GHz Intel Core-i7-7500U, Intel HD 620)</td>\n\t\t\t<td>5,448</td>\n\t\t\t<td>3,911</td>\n\t\t\t<td>E2,791 / P1,560</td>\n\t\t\t<td>3,013</td>\n\t\t\t<td style=\"text-align: left;\">1.67 GB/s / 1.44 GB/s</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"text-align: left;\"><a href=\"https://www.engadget.com/2016/02/25/razer-blade-stealth-review/\">Razer Blade Stealth</a> (2.5GHz Intel Core i7-6500U, Intel HD 520)</td>\n\t\t\t<td>5,131</td>\n\t\t\t<td>3,445</td>\n\t\t\t<td>E2,788 / P1,599 / X426</td>\n\t\t\t<td>3,442</td>\n\t\t\t<td style=\"text-align: left;\">1.5 GB/s / 307 MB/s</td>\n\t\t</tr>\n\t\t<tr>\n\t\t</tr>\n\t</tbody>\n</table>\n</center>\n\n<p>The Compact Desktop is actually made out of laptop hardware. It's powered by an Intel Core i7-7820HK CPU, which is unlocked to make it easier to overclock. It also features NVIDIA's GTX 1080 notebook GPU (overclocked out of the box) with 8GB of video RAM. Additionally, the Compact Desktop packs in 16GB of DDR4 memory and a 1TB SSD. Clearly, HP didn't skimp on components -- this thing can easily take on full-sized gaming rigs.</p>\n\n<p>In <em>Gears of War 4</em> running in 4K with high settings, it managed an impressive average framerate of 56FPS. Stepping down to 1,440p, it reached 90FPS with Ultra settings, and 121 FPS in 1080p. Basically, it'll handle every modern game without trouble. Given its diminutive size, it could also serve as a home theater PC that can blow away the latest 4K ready consoles, like the Xbox One X and PlayStation 4. (Of course, that should be expected when it costs five times as much.)</p>\n\n<p>The desktop also has a built-in battery, which lasted 1 hour and 10 minutes while running the PC Mark 8 benchmark. Clearly, it's not something you're meant to use unplugged for very long. It's still convenient though, since it means you can connect the Compact Desktop to the VR backpack accessory, and swap out extra external batteries, without shutting it down. That's a simple thing you can't do with other VR backpack systems.</p>\n\n<section data-eng-breakout=\"75839\" data-eng-breakout-type=\"e2ehero\" style=\"outline: 2px dashed red;\"><img class=\"e2ehero-url\" data-eng-breakout-field=\"img_url\" data-eng-required=\"true\" src=\"https://o.aolcdn.com/images/dims?crop=1600%2C997%2C0%2C0&amp;quality=85&amp;format=jpg&amp;resize=1600%2C997&amp;image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2F591ba655ffe4437855544c0db9e60a7c%2F205912893%2FHP%2BVR%2Bgallery%2B27.jpg&amp;client=a1acac3e1b3290917d92&amp;signature=4f5322a30b29559ee69a832357e1ec56fb70b126\" />\n<p class=\"e2ehero-credit\" data-eng-breakout-field=\"credit\">Devindra Hardawar/AOL</p>\n</section>\n\n<h3>Using virtual reality</h3>\n\n<p>In desktop mode, HP's mixed reality headset was a cinch to set up. All you have to do is plug in an HDMI and USB cable. There aren't any sensors to install, like with the Oculus Rift and Vive. Everything on Microsoft's VR platform relies on built-in sensors, instead.</p>\n\n<p>When you put on the headset, you're thrown into the Windows Mixed Reality Portal, which is where all of the VR magic happens. You'll be asked how you want to use your headset: in walking mode, which replicates the room-scale VR we've seen on the HTC Vive, or sitting and standing in place. If you choose the latter, you can immediately start moving around Microsoft's virtual living room and testing out mixed reality apps. If you want to walk around VR environments, though, you'll need to clear out nearby furniture and trace a virtual boundary using the headset first. It's all meant to keep you from bumping into your desk and nearby walls.</p>\n\n<p><img data-credit=\"Devindra Hardawar/AOL\" data-mep=\"2266231\" src=\"https://o.aolcdn.com/images/dims?crop=1600%2C900%2C0%2C0&amp;quality=85&amp;format=jpg&amp;resize=1600%2C900&amp;image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2F43bd72c971d69bf23114c8489b52588%2F205912898%2FHP%2BVR%2Bgallery%2B30.jpg&amp;client=a1acac3e1b3290917d92&amp;signature=af7b06b00a59c26ad8b3c1e374307ed128f69333\" alt=\"\" /></p>\n\n<p>Compared to the virtual living rooms from Oculus and HTC Vive, which serve as a home base for everything you're doing in VR, Microsoft's feels comfortable. And even though there are only 60 virtual reality apps in the Windows Store so far, including notable entries like <em>Superhot </em>and <em>Arizona Sunshine</em>, there's still plenty to do. <em>Superhot</em>, feels just as smooth and immersive as it did on the HTC Vive. And since the headset is higher res, everything looked sharper as well. Watching trailers in the virtual screening room almost felt like I was in a theater. And it handled 360-degree videos well. The <em>Star Wars Rogue One </em>behind-the-scenes experience felt just as immersive as other headsets.</p>\n\n<p>Microsoft also wisely partnered with Valve to bring SteamVR over to Mixed Reality headsets. Steam automatically recognized HP's model when I started it up, and I was able to hop into Rez Infinite. After playing for a while, though, it's clear that Microsoft's VR controllers aren't nearly as ergonomic as Oculus's excellent Touch controllers. Hitting the trigger and grip buttons don't feel very natural, and since they have straight handles, they don't fit easily into the natural curve of your hands. Hopefully, that's something Microsoft can fix with its next controllers.</p>\n\n<p><img src=\"https://s.aolcdn.com/hss/storage/midas/6b8eed7d8c4c47320317e48fc77915f8/205913527/HP+VR+Backpack+smaller+2.gif\" /></p>\n\n<p>When it comes to HP's backpack accessory, I was honestly surprised how much I enjoyed using it. It made diving into VR more immersive, since I didn't have to worry about getting tangled in any cables tied to a large desktop. Sure, the setup is a bit more involved: You've got to attach the compact desktop, slide in the battery packs, and make sure everything is connected properly. The backpack feels surprisingly comfortable to wear, thanks to its padded shoulder straps and two front straps. The entire setup clocks in at 8.3 pounds, which isn't much more than what I typically lug around every day in my backpack.</p>\n\n<p><iframe allow=\"encrypted-media\" allowfullscreen=\"\" frameborder=\"0\" gesture=\"media\" height=\"360\" src=\"https://www.youtube.com/embed/pzG7Wc6mbwE\" width=\"640\"></iframe></p>\n\n<p>While playing <em>Superhot</em>, I was able to dodge bullets and take out bad guys far more easily, since I was free to move and bend in ways I couldn't with a typical VR setup. Of course, there's also the danger of hitting a wall and running into furniture. Even if you set up virtual borders, it's easy to miss those when you're swept up in the game world. And, oddly enough, you can't quickly set up new borders in backpack mode -- you can only do it in desktop mode with a monitor attached. So while you can conceivably take the entire VR backpack setup anywhere -- as I did around our offices -- you're stuck using it without anything to warn you about walls or obstacles.</p>\n\n<p>HP claims each pair of batteries adds one hour of juice to the backpack setup, on top of what you get from its built-in power source. In my testing, 15 minutes of gaming typically used up around 10 percent of battery life. (I wasn't able to stay in VR long enough to drain the batteries completely.) Of course, that timing will depend on what, exactly, you're doing. Sitting back and watching a video, or just browsing the web, could stretch the battery life longer.</p>\n\n<p><img data-credit=\"Devindra Hardawar/AOL\" data-mep=\"2266232\" src=\"https://o.aolcdn.com/images/dims?crop=1600%2C920%2C0%2C0&amp;quality=85&amp;format=jpg&amp;resize=1600%2C920&amp;image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2Fb9d05e4c6c8c5117b864818ca05e6ba1%2F205912899%2FHP%2BVR%2Bgallery%2B32.jpg&amp;client=a1acac3e1b3290917d92&amp;signature=1bfca5856b2b57a5049fce2382bfb08ba4ecf3da\" alt=\"\" /></p>\n\n<p>But, I can't help but be a VR backpack sceptic. Wireless VR solutions are already here, and they're only going to get better over the next year. VR backpacks are already an incredibly niche category, but it won't be too long until they're completely unnecessary.</p>\n\n<p>If you want to use HP's headset with the backpack, you'll also have to pick up a $10 virtual display dongle. That's due to an issue with Windows, which simply doesn't spit out an image to mixed reality headsets unless it detects a connected display. While it would make more sense for HP just to include one of those adapters in the box, the company says it's hoping Microsoft comes up with a software fix instead. Oddly, the backpack setup will work fine with an HTC Vive without that workaround.</p>\n\n<h3>Pricing and the competition</h3>\n\n<p><img data-credit=\"Devindra Hardawar/AOL\" data-mep=\"2266233\" src=\"https://o.aolcdn.com/images/dims?crop=1600%2C960%2C0%2C0&amp;quality=85&amp;format=jpg&amp;resize=1600%2C960&amp;image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2F287c2c6a8647376cb4e017b01f511029%2F205912878%2FHP%2BVR%2Bgallery%2B13.jpg&amp;client=a1acac3e1b3290917d92&amp;signature=c4d9862d74fb442bcbcb8cb9448c50a7cf8ebbe1\" alt=\"\" />You'd really need a lot of extra spending money to dive into HP's VR ecosystem. The compact desktop costs $2,499, while its mixed reality headset is an additional $449. And don't forget about the backpack, which adds another $499. It's so costly, that it's even out of the realm of many early adopters. It's more suited for developers who want to explore what's possible with portable VR.</p>\n\n<p>There are, of course, other VR backpack options on the market, like those from Zotac and MSI. They're all bigger and clunkier than HP's system, but at least they're not as expensive. Zotac's VR Go starts at $1,800, and it includes both the desktop and backpack accessory. You'll still need to add your own headset, though.</p>\n\n<h3>Wrap-up</h3>\n\n<section data-eng-breakout=\"78265\" data-eng-breakout-type=\"image\" style=\"outline: 2px dashed red;\"><img class=\"image-url\" data-eng-breakout-field=\"img_url\" data-eng-required=\"true\" src=\"https://o.aolcdn.com/images/dims?crop=1600%2C927%2C0%2C0&amp;quality=85&amp;format=jpg&amp;resize=1600%2C927&amp;image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2F4b93c3d9f5054d1780388c0563642a95%2F205912892%2FHP%2BVR%2Bgallery%2B28.jpg&amp;client=a1acac3e1b3290917d92&amp;signature=a5164ca6a18e60f0b90fd754a58dd3a9be1cfce0\" />\n<p class=\"image-credit\" data-eng-breakout-field=\"credit\">Kris Naudus/AOL</p>\n</section>\n\n<p>Ultimately, the Omen X Compact Desktop's power and unique capabilities helps it stand out from the gaming crowd. HP's mixed reality headset, meanwhile, is a solid entry into new territory, one that's bolstered by Microsoft's growing VR platform. And even though VR backpacks might not be around for long, and they're certainly not something most people should consider, HP's entry remains the best one we've seen so far.</p>\n\t\t\t\t\t\t\t\t\t\t\t\t\t"
                        },
                        "category": [
                            "av",
                            "entertainment",
                            "gadgetry",
                            "gadgets",
                            "gaming",
                            "gear",
                            "hp",
                            "hpmixedreality",
                            "hpvrbackpack",
                            "omenxcompactdesktop",
                            "review"
                        ],
                        "creator": {
                            "__prefix": "dc",
                            "__cdata": "Devindra Hardawar"
                        },
                        "pubDate": "Mon, 04 Dec 2017 09:00:00 -0500",
                        "identifier": {
                            "__prefix": "dc",
                            "__text": "21|23294508"
                        }
                    },
                    {
                        "title": {
                            "__cdata": "OnePlus launches 'Star Wars' 5T in time for 'The Last Jedi'"
                        },
                        "link": "https://www.engadget.com/2017/12/04/oneplus-5t-star-wars-limited-edition/",
                        "guid": {
                            "_isPermaLink": "false",
                            "__text": "https://www.engadget.com/2017/12/04/oneplus-5t-star-wars-limited-edition/"
                        },
                        "comments": "https://www.engadget.com/2017/12/04/oneplus-5t-star-wars-limited-edition/#comments",
                        "description": {
                            "__cdata": "\n\t\t\t\t\t\t<img src=\"https://s.aolcdn.com/hss/storage/midas/11c3a02b2169e43445850aa9aa770278/205917108/oneplus-star-wars-2017-12-04-02.jpg\" /><p>As <a href=\"https://www.engadget.com/2017/10/09/star-wars-the-last-jedi-trailer/\"><em>Star Wars: The Last Jedi</em></a> approaches its December 15th theatrical release, get ready to see the power of Disney's fully armed and operational merchandising machine. OnePlus has <a href=\"https://oneplusstore.in/\">revealed</a> that it will be part of that circus with the 5T <em>Star Wars</em> Limited Edition smartphone. Coming to India on December 14th, it's bedecked with a red side button, backside Star Wars logo and screen theme featuring First Order stormtrooper wallpaper. </p>\n\t\t\t\t\t\t\t<p>OnePlus teased the device via at the Bengaluru Comic Con in India, and will unveil it in Mumbai at a popup store on December 14th, with ticket sales kicking off on Thursday. In a teaser video (below) it also revealed what looks like a new, <a href=\"https://s.aolcdn.com/hss/storage/midas/13a7966c088f9393f9304ba8f0c11e48/205917181/2017-12-04+12+18+33.jpg\">more geometric font</a>, but that might be strictly part of the <em>Star Wars</em> branding tie-in. </p>\n\n<p>Disney has done smartphone merchandising deals before, most notably with Japan's Softbank on a special-edition <a href=\"https://www.engadget.com/2016/11/26/star-wars-phone/\"><em>Star Wars: Rogue One</em> device</a>. However, OnePlus presents itself as a company that can offer premium smartphones on the cheap because it's lean and mean, so it's a bit weird to see it splurging on what must be an expensive <em>Star Wars</em> license -- even if it's only in India. On the other hand, there's no doubt that the Special Edition 5T pushes all the buttons for Android and <em>Star Wars</em> fans, two groups that have a lot of <a href=\"https://www.engadget.com/tag/starwars,android/\">crossover</a>. There's no word on whether it'll arrive in other regions.</p>\n\n<p style=\"text-align: center;\"><iframe allowfullscreen=\"\" frameborder=\"0\" height=\"360\" src=\"https://www.youtube.com/embed/9II-OyakdxE\" width=\"640\"></iframe></p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Via: </strong><a target=\"_blank\" href=\"https://www.theverge.com/circuitbreaker/2017/12/4/16732826/oneplus-5t-star-wars-edition-the-last-jedi\">The Verge</a><!--//--></p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Source: </strong><a target=\"_blank\" href=\"https://oneplusstore.in/starwars\">OnePlus</a><!--//--></p>\n\t\t\t\t\t\t\t"
                        },
                        "category": [
                            "disney",
                            "gear",
                            "india",
                            "merchandise",
                            "mobile",
                            "oneplus",
                            "oneplus5t",
                            "starwars",
                            "thelastjedi",
                            "video"
                        ],
                        "creator": {
                            "__prefix": "dc",
                            "__cdata": "Steve Dent"
                        },
                        "pubDate": "Mon, 04 Dec 2017 08:31:00 -0500",
                        "identifier": {
                            "__prefix": "dc",
                            "__text": "21|23296075"
                        }
                    },
                    {
                        "title": {
                            "__cdata": "GAME hooks up with Three to sell more smartphones"
                        },
                        "link": "https://www.engadget.com/2017/12/04/game-three-uk-smartphone-trial/",
                        "guid": {
                            "_isPermaLink": "false",
                            "__text": "https://www.engadget.com/2017/12/04/game-three-uk-smartphone-trial/"
                        },
                        "comments": "https://www.engadget.com/2017/12/04/game-three-uk-smartphone-trial/#comments",
                        "description": {
                            "__cdata": "\n\t\t\t\t\t\t<img src=\"https://o.aolcdn.com/images/dims?crop=3500%2C2306%2C0%2C0&amp;quality=85&amp;format=jpg&amp;resize=1600%2C1054&amp;image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2F3c9afb17b46d3f0887b3205120737e06%2F205917368%2Fview-of-the-empty-game-store-in-wardour-street-london-on-black-friday-picture-id878275634&amp;client=a1acac3e1b3290917d92&amp;signature=2a27e8a722d771ec6559f1fb8185c04e67e0efec\" /><p>GAME is trying everything to turn its struggling retail business around. Today, the company <a href=\"http://www.gamesindustry.biz/articles/2017-12-04-game-partners-with-three-for-in-store-smartphone-push\">announced a partnership with Three</a> that will expand its in-store phone offerings. A Three-branded \"gondola\" will be trialed in 15 London stores, pushing smartphones, SIM cards, MiFi devices and pay-as-you-go top-ups. GAME sells handsets already but typically these are trade-ins rather than fresh devices. Clearly, this is an attempt to broaden the company's wares and reverse <a href=\"http://www.gamedigitalplc.com/media-centre/press-releases/pr-2017/15-nov-2017\">its falling revenue</a>, which dropped from &pound;813 million to &pound;783 million in the 12 month period leading up to July.</p>\n\t\t\t\t\t\t\t<p>GAME knows it's in trouble. Twelve months ago, it launched <a href=\"http://www.gamesindustry.biz/articles/2016-12-15-game-launches-belong-is-this-the-future-of-video-games-retail\">a series of experience stores</a> in London and Milton Keynes branded \"Belong.\" They included tiny \"arenas\" &mdash; arcades for the eSports generation &mdash; that hosted tournaments and other casual events. It's unclear if the concept has been successful, however. Last month, Game <a href=\"http://www.mcvuk.com/articles/retail/game-sells-multiplay-digital-to-unity\">sold part of Multiplay</a> &mdash; a company <a href=\"https://www.engadget.com/2015/03/02/game-multiplay-esports/\">it acquired for &pound;20 million in 2015</a> &mdash; to engine developer Unity for &pound;19 million. Specifically, it ditched Multiplay's server hosting division, keeping the portion that runs its Belong arenas and the gaming festival Insomnia.</p>\n\n<p>All of this masks the real problem facing GAME: digital sales. Every year more people are choosing to buy software through Steam, PSN, the Xbox Store, and Nintendo's eShop. Downloads are more convenient and a well-timed sale can beat the prices offered by GAME or even online retailers such as Amazon. There is, of course, still an interest in boxed copies &mdash; they make great gifts, you can share them with friends, and you can save money by trading them in or selling them on afterwards. The industry is shifting, however, and GAME needs a real, long-term solution to survive.</p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Via: </strong><a target=\"_blank\" href=\"http://www.gamesindustry.biz/articles/2017-12-04-game-partners-with-three-for-in-store-smartphone-push\">Gamesindustry.biz</a><!--//--></p>\n\t\t\t\t\t\t\t\t\t\t"
                        },
                        "category": [
                            "business",
                            "game",
                            "gameuk",
                            "gaming",
                            "three"
                        ],
                        "creator": {
                            "__prefix": "dc",
                            "__cdata": "Nick Summers"
                        },
                        "pubDate": "Mon, 04 Dec 2017 08:25:00 -0500",
                        "identifier": {
                            "__prefix": "dc",
                            "__text": "21|23296194"
                        }
                    },
                    {
                        "title": {
                            "__cdata": "Facebook rolls out a Messenger app just for kids"
                        },
                        "link": "https://www.engadget.com/2017/12/04/facebook-messenger-kids/",
                        "guid": {
                            "_isPermaLink": "false",
                            "__text": "https://www.engadget.com/2017/12/04/facebook-messenger-kids/"
                        },
                        "comments": "https://www.engadget.com/2017/12/04/facebook-messenger-kids/#comments",
                        "description": {
                            "__cdata": "\n\t\t\t\t\t\t<img src=\"https://img.vidible.tv/prod/2017-12/04/5a25a83fd0e3cf56235cc483/5a25a88f75a8046503722788_o_U_v1.jpg\" /><div class=\"vdb_player vdb_5706c5c8e4b03b51471aefba564f3144ff690c0a7c285e51\" data-placeholder=\"//img.vidible.tv/prod/2017-12/04/5a25a83fd0e3cf56235cc483/5a25a88f75a8046503722788_o_U_v1.jpg?w=1440&amp;h=900\" id=\"5706c5c8e4b03b51471aefba\" vdb_params=\"m.embeded=cms_video_plugin_cms.publishing.oath.com\"><img src=\"https://img.vidible.tv/prod/2017-12/04/5a25a83fd0e3cf56235cc483/5a25a88f75a8046503722788_o_U_v1.jpg\" style=\"display:none;\" />\n\n</div>\n\n<p>It's no surprise that kids are using technology at a pretty young age these days, and a key part of that is using it to communicate with friends and family. But most kids tend to use tablets or iPod Touches that don't have phone numbers, so normal texting and video chats are a no-go (unless it's with mommy's phone, which isn't always great for mommy). Sure you could use an app like FaceTime or Hangouts, but most of them don't offer the kind of control that parents want, especially for really young kids. Facebook, however, has come up with a solution. It's called Messenger Kids and yes, it's basically a standalone child-friendly <a href=\"https://www.engadget.com/2017/11/21/facebook-messenger-improved-photo-quality/\">Messenger</a> app with parental controls baked right in.</p>\n\t\t\t\t\t\t\t<p>Before you balk at the idea of having to sign your kid up on Facebook, don't worry -- you won't have to. In fact, your kid doesn't need a Facebook account at all in order to use Messenger Kids. You, however, do. That's because it uses the parent's Facebook account to set up the child's Messenger Kids account.</p>\n\n<p>Here's how it works. When you download the Messenger Kids app, the first thing you'll do is authorize it with your own Facebook account. This does not log you into the tablet -- all it does is use your account to authenticate it. You then create a completely separate Messenger Kids account with your child's first and last name. Again, this does not create a Facebook account for the child, and their name will not be publicly searchable. Oh, and don't worry, Messenger Kids is also ad-free.</p>\n\n<p>So how is your kid supposed to make friends on Messenger Kids? Well, the only way they can do that is through you. The parents are the only people who can add contacts for their kids. And the only way they can do that is through Facebook. Adding adult family members like grandpa and grandma is easy, because they likely already have Facebook accounts. Adults can use the normal Messenger app to talk to the kids on Messenger Kids -- it's completely interoperable.</p>\n\n<p>But let's say your child wants to befriend other children, like his classmate Peggy, on Messenger Kids. What then? In this case, you have to already be Facebook friends with Peggy's parents. If Peggy is already on Messenger Kids, you'll see her name under her parent's name and you can tap it to add her as a friend to your child. Peggy's parents will then have to approve your contact request in order to add your child to Peggy's friends list.</p>\n\n<p>And, of course, parents can remove Messenger Kids contacts at any time from their own account, and the child cannot reinstate an account that have been removed. On top of that, Messenger Kids has report and block controls within the app itself, so kids can report and block people if they want. And, any time a child reports or blocks anyone, their parents will get a notification letting them know about it. Loren Cheng, a product manager at Facebook who led the Messenger Kids project, said that the company actually has a dedicated team at Facebook who looks at all the reports, and will proactively ban any account that is seen as harmful.</p>\n\n<p>As for the app itself, it's designed to be very visually oriented. The home screen has big thumbnail shortcuts to frequent contacts like friends and family, with a heavy emphasis on real-time video chat. \"Kids really like real-time communication,\" said Cheng. \"They want to be in the moment, back and forth [...] Kids see communication as play.\" Of course, Messenger Kids also allows for normal text-based communications as well, just like regular Messenger.</p>\n\n<p>That is also why the video chat on Messenger Kid is loaded with emojis, GIFs, selfie frames and stickers, all of which have been screened to be age-appropriate. In fact, Facebook actually built a custom art team just for Messenger Kids, and worked with child development experts across the country to come up with what would be good for kids at this age, developmentally, and what would be good for families. One particular photo mask, for example, is an AR experience that helps educate kids about vaquitas, an endangered species of dolphins, and immerses them in a scuba-diving environment.</p>\n\n<p>Facebook told us that it took great pains to reach out to child development experts such as the National PTA, the Center of Media and Child Health, the American Association of Pediatrics and many more as part of an Advisory Committee when developing Messenger Kids.</p>\n\n<p>\"What we learned is that parents want more control over who their kids have contacts with,\" said Antigone Davis, Facebook's head of safety. \"They want the content to be appropriate, and they want to have better control over time spent on these technologies.\" At the same time, Davis said that parents also want their children to be digitally literate, and to build the skills necessary to communicate in the modern world.</p>\n\n<p>\"Messenger Kids was designed to bring families together for conversations, as kids learn how to navigate the digital world,\" said Cheng.</p>\n\n<p>Messenger Kids is available for preview in the US starting today. It's iOS-only at launch, but will roll out to Android users later this month.</p>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<p><strong>Source: </strong><a target=\"_blank\" href=\"https://newsroom.fb.com/news/2017/12/introducing-messenger-kids-a-new-app-for-families-to-connect/\">Facebook</a><!--//--></p>\n\t\t\t\t\t\t\t"
                        },
                        "category": [
                            "aoloriginals",
                            "dailyshow",
                            "engadget",
                            "engadgettoday",
                            "engadgetvideo",
                            "entertainment",
                            "facebook",
                            "facebookmessenger",
                            "gear",
                            "internet",
                            "kerrydavis",
                            "messengerkids",
                            "mobile",
                            "video"
                        ],
                        "creator": {
                            "__prefix": "dc",
                            "__cdata": "Nicole Lee"
                        },
                        "pubDate": "Mon, 04 Dec 2017 08:00:00 -0500",
                        "identifier": {
                            "__prefix": "dc",
                            "__text": "21|23296180"
                        }
                    },
                    {
                        "title": {
                            "__cdata": "VW unveils an electric van for its MOIA ride-sharing service"
                        },
                        "link": "https://www.engadget.com/2017/12/04/vw-moia-ride-sharing-electric-van/",
                        "guid": {
                            "_isPermaLink": "false",
                            "__text": "https://www.engadget.com/2017/12/04/vw-moia-ride-sharing-electric-van/"
                        },
                        "comments": "https://www.engadget.com/2017/12/04/vw-moia-ride-sharing-electric-van/#comments",
                        "description": {
                            "__cdata": "\n\t\t\t\t\t\t<img src=\"https://s.aolcdn.com/hss/storage/midas/81c09b0780d3ab2ad239e604c414640a/205916926/volkswagen-moia-ride-sharing-2017-12-04-05.jpg\" /><p>Volkswagen has <a href=\"https://www.moia.io/blog/highlights/techcrunch/\">unveiled</a> the electric van that's a key part of its <a href=\"https://www.engadget.com/2016/12/05/vw-creates-moia-mobility-company/\">MOIA</a> autonomous vehicle ride-sharing service. Shown yesterday at <a href=\"https://techcrunch.com/2017/12/04/volkswagens-moia-debuts-its-all-electric-rideshare-vehicle/\"><em>TechCrunch</em> Disrupt</a>, it'll carry up to six passengers with niceties like roomy individual seats, ambient LED lighting, WiFi and device power ports. The van-pooling MOIA service will launch in Hamburg in 2018 with 200 vans, letting passengers enter a departure point and destination in an app. \"We've set ourselves the goal of taking more than a million cars off the roads in Europe and the USA by 2025,\" said MOIA CEO Ole Harms.</p>\n\t\t\t\t\t\t\t<p>MOIA's aim is to eventually put autonomous, purpose-built vehicles on the road without drivers. For its first electric van, however, the company isn't emphasizing the self-driving part. Rather, it's focusing on electric vehicle benefits, including the pollution-free 300 km (186 mile) range, 30-minute time to an 80 percent charge and quiet operation. At Disrupt, the company said that the business can still be profitable without the need for autonomous operation.</p>\n\n<p style=\"text-align: center;\"><img data-credit=\"MOIA\" src=\"https://s.aolcdn.com/hss/storage/midas/1ba7356d06dfa5b69a5b5c24127aa1dd/205917001/volkswagen-moia-ride-sharing-2017-12-04-04.jpg\" data-mep=\"2269258\" alt=\"\" data-provider=\"other\" data-provider-asset-id=\"205917001\" /></p>\n\n<p>MOIA also revealed more details about the ride-pooling service, including pricing. While it'll obviously be cheaper than calling your own taxi or Lyft, it won't undercut public transport. \"We are operating with full respect of the public system,\" Harms told TechCrunch. \"We don't want to get below the public transport system because them we would take people who are already pooled in a bigger vessel into a smaller one.\"</p>\n\n<p>VW was able to get the MOIA van on the road and present a functional vehicle in just ten months thanks to \"agile\" design techniques used at its factory in Onsnabr&uuml;ck. It has already incorporated passenger feedback from tests that started in October using the Volkswagen T6 van. To make the carpooling app work efficiently, the company is developing virtual \"bus stops\" every 250 yards or so in cities where it'll operate.</p>\n\n<p>Many automakers, including <a href=\"https://www.engadget.com/2017/01/13/volvo-global-car-sharing-service/\">Volvo</a>, <a href=\"https://www.engadget.com/2017/07/27/ford-chariot-rideshare-service-shuttle-crowdsourced-new-york-city/\">Ford</a> and <a href=\"https://www.engadget.com/2017/02/17/gm-and-lyft-could-deliver-a-self-driving-fleet-next-year/\">GM</a>, plan to launch their own ride-sharing services, either alone or in partnership with companies like Uber and Lyft. Volkswagen is now pretty far ahead of the game, however, with a purpose built vehicle and launch date (it'll come to Europe by the end of next year and the US in 2025). \"Many ideas have already been integrated into the development of this vehicle,\" said MOIA COO Robert Henrich. \"But parallel to this, we're also working on future versions of the vehicle.\"</p>\n\n<p style=\"text-align: center;\"><img data-credit=\"VW\" src=\"https://s.aolcdn.com/hss/storage/midas/98669581630dfc24085b69b108015998/205917004/volkswagen-moia-ride-sharing-2017-12-04-02.jpg\" data-mep=\"2269259\" alt=\"\" data-provider-asset-id=\"205917004\" /></p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Via: </strong><a target=\"_blank\" href=\"https://techcrunch.com/2017/12/04/volkswagens-moia-debuts-its-all-electric-rideshare-vehicle/\">TechCrunch</a><!--//--></p>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Source: </strong><a target=\"_blank\" href=\"https://www.moia.io/blog/highlights/techcrunch/\">MOIA</a><!--//--></p>\n\t\t\t\t\t\t\t"
                        },
                        "category": [
                            "electricvehicle",
                            "ev",
                            "gear",
                            "moia",
                            "ridesharing",
                            "self-driving",
                            "transportation",
                            "volkswagen",
                            "vw"
                        ],
                        "creator": {
                            "__prefix": "dc",
                            "__cdata": "Steve Dent"
                        },
                        "pubDate": "Mon, 04 Dec 2017 07:10:00 -0500",
                        "identifier": {
                            "__prefix": "dc",
                            "__text": "21|23296009"
                        }
                    }
                ]
            },
            "_xmlns:dc": "https://purl.org/dc/elements/1.1/",
            "_xmlns:itunes": "https://www.itunes.com/dtds/podcast-1.0.dtd",
            "_version": "2.0"
        }
    }

;