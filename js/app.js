
//Make sure all data, files, etc, are completely and entire loaded:
$(window).on('load', function () {
    //Echo Obj @ data/json.js:
    //console.log(rssContent);

    //OK. Here we go!
    //Give it a go to the items we have: get the LATEST 10 items (sort date desc => pick 1st 10), as the trial demands:
    const
        items          = rssContent.rss.channel.item
        , max_nItems   = 10
        //The property where date is, on each item:
        , date_field   = 'pubDate'
        //The name of an extra property, we'll add to each of 'items', having date (as a string) translated to a JS date format:
        , JSdate_field = 'js_date'
    ;
    var
        //An array where all 'items' will be collected for later HTML printing:
        arrayItems = []

    ;

    //1st, to sort after, you need to get each item into an array:
    arrayItems = add_JSDateProp(items, date_field, JSdate_field);
    
    //Sort it, desc, by field 'JSdate_field':
    sortByProperty(arrayItems, JSdate_field);
    /*
    arrayItems.sort(JSdate_field, function(a,b){
        return a[JSdate_field] <=  b[JSdate_field] ? 1 : -1;        
    });
    */

    //Only want 1st 10...?
    arrayItems = arrayItems.slice(0, max_nItems);
    console.warn(arrayItems);

    //Do each grid cell with each of 'arrayItems' item, already sorted and sliced to Top 'max_nItems':
    do_itemsGrid(arrayItems);

})


/**
 * 
 * @param {Obj} dateObj         // An Obj with 'date_field' as a date property 
 * @param {string} dateProp     // The date property name, passing in as a string
 * @param {date} JSdateProp     // The JS date property name, that will be added to each of 'dateObj' ('items' collection)
 */
function add_JSDateProp(dateObj, dateProp, JSdateProp){
    var
        arrayDates = []
    ;

    //Transverse items: 
    Object.keys(dateObj).forEach(function(item_key, item_idx) {
        const
            eachItem  = dateObj[item_key]
            , strDate = eachItem[dateProp]
            //And it's parts, so we can convert it to a JS date to compare (latest!)
            , strDate_date = strDate.split(',')[1].trim()
            , jsDate       = new Date(strDate_date)

        ;
        //console.log(eachItem);
        //Add a new property to each: a JS date (NOTE: JS translates to GMT: "Mon, 04 Dec 2017 08:31:00 -0500" => Mon Dec 04 2017 13:31:00 GMT+0000 (Hora padrão de GMT))
        eachItem[JSdateProp] = jsDate
        //Do an array with each, so it can be sorted after:
        arrayDates.push(eachItem)

    });

    return arrayDates;

}


/**
 * 
 * @param {array} array 
 * @param {string} propertyName     //The property from which the sort is made.
 */
function sortByProperty(array, propertyName) {
    return array.sort(function (a, b) {
        return a[propertyName] - b[propertyName];
    });
}


/**
 * 
 * @param {array} arrayFeeds        //Draw the HTML's grid of the collection of feeds (fetched in 'items' collection ok Objs) from 'arrayFeeds'
 */
function do_itemsGrid(arrayFeeds){
    const
        $grid = $('main div.container-of-feeds')
        
    ;
    //Transverse each feed:
    for (var nCell = 0; nCell < arrayFeeds.length; nCell++) {
        const
            gridCell      = arrayFeeds[nCell]
            , description = gridCell.description.__cdata
            , title       = gridCell.title.__cdata
            , linkTitle   = gridCell.link
            , pubDate     = gridCell.pubDate
            , author      = gridCell.creator.__cdata
            //Collection of categories:
            , categories  = gridCell.category

            //Concerning DOM, we will have a Description, containing an Img:
            , $description = $(description)
            , $desc_img    = $description.filter('img')
            //and a collection of paragraphs, below each Img, also inside each Description:
            , $desc_p      = $description.filter('p')
        
        ;
        //Must collect outerHTML of each of the colection above:
        var
            desc_p_HTML       = ''
            , categories_HTML = ''
        ;
        $desc_p.each(function(){
            desc_p_HTML += $(this)[0].outerHTML;
        })
        categories.forEach(function(elem){
            const
                randomNumber = Math.floor(Math.random()*6)

            ;
            var
                strBadgeClass = ''
            
            ;
            switch(randomNumber) {
                case 0:
                   strBadgeClass = 'label-default'
                   break;
                case 1:
                   strBadgeClass = 'label-primary'
                   break;
                case 2:
                   strBadgeClass = 'label-success'
                   break;
                case 3:
                   strBadgeClass = 'label-info'
                   break;
                case 4:
                   strBadgeClass = 'label-warning'
                   break;
                case 5:
                   strBadgeClass = 'label-danger'
                   break;
            } 
            categories_HTML += '<a href="JavaScript: alert(\'Filter by ' +elem+ '\');" class="label ' +strBadgeClass+ '">' +elem+ '</a>';
        })
        //console.warn(desc_p_HTML);

        //Do each cell (a BS column) with a BS panel collapse inside:
        $grid.append(
            '<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">' +

                '<div class="img-container">' + $desc_img[0].outerHTML + '</div>' +

                '<div class="panel-group">' +
                    '<div class="panel panel-default">' +

                        '<div class="panel-heading">' +
                           '<h4 class="panel-title">' +
                               '<a class="text-link" href="' +linkTitle+ '" target="_blank">' +title+ '</a>' +
                            '</h4>' +
                        '</div>' +

                        '<div id="collapse-NOT-' +nCell+ '" class="panel-collapse collapse in">' +
                            desc_p_HTML +
                        '</div>' +

                        '<div class="panel-footer panel-categories">' +categories_HTML+ '</div>' +

                        '<div class="panel-footer panel-roda-pe">' +
                            '<p>' +pubDate+ '</p>' +
                            '<p>' +
                                '<span class="pull-left">' + author + '</span>' +
                                '<a class="pull-right" href="JavaScript: alert(\'Comentários\')"><i class="fa fa-comments" aria-hidden="true"></i></a>' +
                            '</p>' + 
                        '</div>' +

                    '</div>' +
                '</div>' +

            '</div>'
        );

        //Finally we have the DOM re-done. Last, treat each img as BS responsive:
        $grid.children('div')
            .eq(nCell)
                .find('img')
                    .addClass('img-responsive')
            
        ;
    }

}